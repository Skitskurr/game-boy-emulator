#pragma once
#include "../GenericHeader.h"
#include <d3d11.h> // d3d9 is old news dont even think about it
#include <dsound.h> // should be fun.
#include <vector>

class GFX {
	
	ATOM wClass;
	HWND hWindow;
	ID3D11DeviceContext *pCtx = nullptr;
	ID3D11Device		*pDevice = nullptr;
	IDXGISwapChain		*pSwapchain = nullptr;
	DXGI_SWAP_CHAIN_DESC *pDesc = nullptr;
	ID3D11RenderTargetView * pRenderView = nullptr;
	IDirectSound8 * pDirectSound;
	IDirectSoundBuffer* pPrimaryBuffer; // cant get the actual IDirectSoundBuffer8 for primary buffers because directsound is asscakes
	IDirectSoundBuffer8 * pSoundBuffer; // cant get the actual IDirectSoundBuffer8 for primary buffers because directsound is asscakes


	void InitDirectSound( );

	static LRESULT CALLBACK WindowHandler( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam );
	bool InitializeDirectX( int width, int height ); // doesnt really need to be here, but i like them here.
	// take given buffer from GPU and compile it into a DX Texture
	void CompileImage( float** buf );

	int gb_w = 0, gb_h = 0;
	int wnd_w = 0, wnd_h = 0;
	void ClearScreen( );
	bool initialized = false;
	struct PIXEL_VERTEX {
		float x, y, z;
		float col[ 4 ];
		//float * col;
	};
	//PIXEL_VERTEX * pPix = new PIXEL_VERTEX[ size ];
	PIXEL_VERTEX *pPix = nullptr;

public:
	void PlayGBASound( void* buffer, int len );
	void DrawGPUBuffer( float** buf );
	bool SpawnWindow( );

	GFX( int gb_w, int gb_h, int window_w = 0, int window_h = 0 );
};
