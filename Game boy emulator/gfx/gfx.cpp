#include "gfx.hpp"
#include <thread>
#include <d3dcompiler.h>
#include <DirectXMath.h>
#include "../GBDefines.hpp"
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dsound.lib")
#pragma comment(lib, "dxguid.lib" ) // for that 1 IID_IDirectSoundBuffer8 line.

void GFX::InitDirectSound( )
{
	HRESULT hr;
	hr = DirectSoundCreate8( 0, &pDirectSound, 0 );
	if ( FAILED( hr ) )
	{
		__debugbreak( );
		return;
	}

	hr = pDirectSound->SetCooperativeLevel( this->hWindow, DSSCL_PRIORITY ); // dunno yet.
	if ( FAILED( hr ) )
	{
		__debugbreak( );
		return;
	}

	DSBUFFERDESC desc = { 0 };
	desc.dwSize = sizeof( desc );
	desc.dwFlags = DSBCAPS_PRIMARYBUFFER | DSBCAPS_CTRLVOLUME;
	desc.dwBufferBytes = 0;
	desc.guid3DAlgorithm = GUID_NULL;
	hr = pDirectSound->CreateSoundBuffer( &desc, ( LPDIRECTSOUNDBUFFER* )&pPrimaryBuffer, 0 );
	if ( FAILED( hr ) )
	{
		__debugbreak( );
		return;
	}

	WAVEFORMATEX wf = { 0 };
	wf.wFormatTag = WAVE_FORMAT_PCM; // dunno what format gameboy is , check this later.
	wf.nSamplesPerSec = 44100;
	wf.wBitsPerSample = 16;
	wf.nChannels = 2;
	wf.nBlockAlign = ( wf.wBitsPerSample / 8 ) * wf.nChannels;
	wf.nAvgBytesPerSec = wf.nSamplesPerSec * wf.nBlockAlign;
	wf.cbSize = 0;

	hr = pPrimaryBuffer->SetFormat( &wf );
	if ( FAILED( hr ) )
	{
		__debugbreak( );
		return;
	}

	LPDIRECTSOUNDBUFFER temp = 0;
	desc = { 0 };
	desc.dwSize = sizeof( desc );
	desc.dwBufferBytes = DSBSIZE_MAX;
	desc.lpwfxFormat = &wf;
	desc.dwFlags = DSBCAPS_CTRLFREQUENCY | DSBCAPS_CTRLVOLUME;

	hr = pDirectSound->CreateSoundBuffer( &desc, &temp, 0 );
	if ( FAILED( hr ) )
	{
		__debugbreak( );
		return;
	}

	hr = temp->QueryInterface( IID_IDirectSoundBuffer8, ( void** )&this->pSoundBuffer );
	if ( FAILED( hr ) )
	{
		__debugbreak( );
		return;
	}

	temp->Release( );
}

void GFX::PlayGBASound( void * buffer, int len )
{
	void* toCopy = nullptr;
	DWORD lockedLen = len;
	HRESULT hr = 0;
	hr = this->pSoundBuffer->Lock( 0, 0, &toCopy, &lockedLen, 0, 0, DSBLOCK_ENTIREBUFFER ); // if lockedLen < len then we need to use the 2nd pointers as well (god this library sucks.)
	if ( FAILED( hr ) )
		__debugbreak( );
	memcpy( toCopy, buffer, len );

	hr = this->pSoundBuffer->Unlock( toCopy, len, 0, 0 );
	if ( FAILED( hr ) )
		__debugbreak( );

	//hr = this->pSoundBuffer->SetVolume( DSBVOLUME_MAX ); // LOL ?
	//if ( FAILED( hr ) )
		//__debugbreak( );

	hr = this->pSoundBuffer->Play( 0, 0, 0 );
	if ( FAILED( hr ) )
		__debugbreak( );
}


LRESULT CALLBACK GFX::WindowHandler( HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	//Log( "MSG: %X", msg );
	switch ( msg )
	{
		case WM_NCDESTROY:
			PostQuitMessage( 0 ); // let our thread know we're dead.
			break;
		case WM_KEYUP:
		case WM_KEYDOWN:
		{
			struct ThanksBill {
				uint32_t repeat_count : 16; // number of times repeated (held key)
				uint32_t scan_code : ( 23 - 16 ); // 16
				uint32_t _pad : ( 30 - 23 ); // 23
				uint32_t previous_state : 1; // 30 // 1 == held before, 0 == was up
				uint32_t transition_state : 1; // 31 // 0 if currently pressed, 1 if now released
			};
			ThanksBill * pState = ( ThanksBill* )&lParam;
			bool up = ( pState->transition_state );

			switch ( wParam ) // w is now vkcode.
			{
				case 'W':
					KeyStruct[ 0 ] = up;
					break;
				case 'A':
					KeyStruct[ 1 ] = up;
					break;
				case 'S':
					KeyStruct[ 2 ] = up;
					break;
				case 'D':
					KeyStruct[ 3 ] = up;
					break;
				case 'Z': // A button
					KeyStruct[ 4 ] = up;
					break;
				case 'X': // B Button
					KeyStruct[ 5 ] = up;
					break;
				case 'Q': // Start
					KeyStruct[ 6 ] = up;
					break;
				case 'E': // Select
					KeyStruct[ 7 ] = up;
					break;
			}
			return 0;
			break;
		}

		case WM_COMMAND:
			switch ( wParam )
			{
				case 0x9C41: // clicked Show Tiles
					// spawn yet another window whos sole purpose it to draw the tiles in the tileset in a neat grid (like bgb)
					break;

				default:
					break;
			}
			break;

		default:
			//Log( "MSG: %X", msg );
			break;
	}

	return DefWindowProcA( hwnd, msg, wParam, lParam );
}

ID3D11VertexShader *pVS;    // the vertex shader
ID3D11PixelShader *pPS;     // the pixel shader
ID3D11Buffer * pConstantBuffer = 0; // constant buffer ( where the projection matrix is placed )
ID3D11InputLayout *pLayout = 0;
bool GFX::InitializeDirectX( int width, int height )
{
	DXGI_SWAP_CHAIN_DESC desc = { 0 };
	desc.OutputWindow = this->hWindow;
	desc.BufferCount = 2; // D O U B L E  B U F F E R D
	desc.BufferDesc.Width = width;
	desc.BufferDesc.Height = height;
	desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM; // should only need an alpha i guess, GB has no color, right?
	desc.BufferDesc.RefreshRate.Numerator = 60;
	desc.BufferDesc.RefreshRate.Denominator = 1; // SIXTY HERTZ BOIS

	desc.SampleDesc.Quality = 0, desc.SampleDesc.Count = 1;

	desc.Windowed = true; // dont set this to false unless you want DX to hijack the fuck out of your monitor
	int createFlags = 0;
	createFlags |= D3D11_CREATE_DEVICE_DEBUG; // for debugging, duh.
	auto success = D3D11CreateDeviceAndSwapChain( 0, D3D_DRIVER_TYPE::D3D_DRIVER_TYPE_HARDWARE, 0,
		createFlags, 0, 0, D3D11_SDK_VERSION, &desc, &this->pSwapchain, &this->pDevice, 0, &this->pCtx );

	if ( success != S_OK )
	{
		Log( "God help us, its failed." );
		return false;
	}

	if ( !this->pDesc )
	{
		this->pDesc = new DXGI_SWAP_CHAIN_DESC( );
		this->pSwapchain->GetDesc( this->pDesc );
	}

	ID3D10Blob *VS, *PS;
	// have to compile shaders here.
#pragma region Shaders
	// so i want t obe able to transform 0,0 to be the top left
	auto shader = R"(
	cbuffer VS_CONSTANT_BUFFER : register( b0 )
	{
		matrix mMat;
	};

	struct VOut
	{
		float4 position : SV_POSITION;
		float4 color : COLOR;
	};

	VOut VShader( float4 position : POSITION, float4 color : COLOR )
	{
		VOut output;
		output.position = mul( mMat, position );
		output.color = color;
		return output;
	}

	float4 PShader( float4 position : SV_POSITION, float4 color : COLOR ) : SV_TARGET
	{
		return float4( color[0], color[1], color[2], color[3] );
	})";

#pragma endregion

	HRESULT hr = D3DCompile( shader, strlen( shader ) + 1, "VS", 0, 0, "VShader", "vs_4_0", 0, 0, &VS, 0 );
	if ( FAILED( hr ) )
		__debugbreak( );
	hr = D3DCompile( shader, strlen( shader ) + 1, "PS", 0, 0, "PShader", "ps_4_0", 0, 0, &PS, 0 );
	if ( FAILED( hr ) )
		__debugbreak( );

	pDevice->CreateVertexShader( VS->GetBufferPointer( ), VS->GetBufferSize( ), NULL, &pVS );
	pDevice->CreatePixelShader( PS->GetBufferPointer( ), PS->GetBufferSize( ), NULL, &pPS );

	D3D11_INPUT_ELEMENT_DESC ied[ ] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
	};

	pCtx->PSSetShader( pPS, 0, 0 );
	pCtx->VSSetShader( pVS, 0, 0 );

	DirectX::XMMATRIX mat = DirectX::XMMatrixOrthographicOffCenterLH( 0, width, height, 0, 0.f, 1.f );
	//D3DXMatrixOrthoOffCenterLH( &mat, 0, width, height, 0, 0.f, .1f ); // was 0 where the 1 is, for some reason it dicks my lines sometimes.

	D3D11_BUFFER_DESC cbDesc;
	cbDesc.ByteWidth = sizeof( mat );
	cbDesc.Usage = D3D11_USAGE_DYNAMIC;
	cbDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	cbDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	cbDesc.MiscFlags = 0;
	cbDesc.StructureByteStride = 0;

	// Fill in the subresource data.
	D3D11_SUBRESOURCE_DATA InitData;
	InitData.pSysMem = &mat;
	InitData.SysMemPitch = 0;
	InitData.SysMemSlicePitch = 0;

	// Create the buffer.
	hr = pDevice->CreateBuffer( &cbDesc, &InitData, &pConstantBuffer );
	if ( FAILED( hr ) )
		__debugbreak( );

	pCtx->VSSetConstantBuffers( 0, 1, &pConstantBuffer );

	hr = pDevice->CreateInputLayout( ied, 2, VS->GetBufferPointer( ), VS->GetBufferSize( ), &pLayout );
	if ( FAILED( hr ) )
		__debugbreak( );

	pCtx->IASetInputLayout( pLayout );

	ID3D11Texture2D * pBackBuffer = 0;
	hr = this->pSwapchain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), ( LPVOID* )&pBackBuffer );
	if ( FAILED( hr ) )
		__debugbreak( );

	hr = pDevice->CreateRenderTargetView( pBackBuffer, 0, &this->pRenderView );
	if ( FAILED( hr ) )
		__debugbreak( );

	pBackBuffer->Release( );
	this->pCtx->OMSetRenderTargets( 1, &pRenderView, 0 );

	D3D11_VIEWPORT viewPort = { 0 };
	viewPort.Width = this->pDesc->BufferDesc.Width;
	viewPort.Height = this->pDesc->BufferDesc.Height;
	viewPort.MaxDepth = 1.0f;
	this->pCtx->RSSetViewports( 1, &viewPort );

	this->InitDirectSound( );

	return true;
}

// TODO: Could use a constant buffer, though resizing may be confusing

void GFX::CompileImage( float** buf )
{
	if ( buf[ 0 ] == nullptr )
		return;// not initializd yet
	if ( !this->initialized )
		return;

	HRESULT hr = 0;
	auto size = this->gb_w * this->gb_h + 1;
	if ( this->pPix == nullptr )
		this->pPix = new PIXEL_VERTEX[ size ];

	int i = 0;

	for ( int y = 0; y < this->gb_h; y++ )
	{
		for ( int x = 0; x < this->gb_w; x++ )
		{
			auto pCol = buf[ i ];
			PIXEL_VERTEX vert = { 0 };
			vert.x = x + 1;
			vert.y = y + 1;
			vert.z = 0;
			//vert.col = pCol;
			memcpy( vert.col, pCol, sizeof( float[ 4 ] ) );
			pPix[ i ] = vert;
			i++;
		}
	}
	// clean screen
	// draw pixels (possibly use a POINTLIST and just set them via XY
	ID3D11Buffer * pVertexBuffer = nullptr;
	D3D11_BUFFER_DESC bd = { 0 };
	bd.Usage = D3D11_USAGE_DYNAMIC;
	bd.ByteWidth = size * sizeof( PIXEL_VERTEX );
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	hr = pDevice->CreateBuffer( &bd, NULL, &pVertexBuffer );       // create the buffer
	D3D11_MAPPED_SUBRESOURCE ms;
	hr = pCtx->Map( pVertexBuffer, NULL, D3D11_MAP_WRITE_DISCARD, NULL, &ms );   // map the buffer
	memcpy( ms.pData, pPix, size * sizeof( PIXEL_VERTEX ) );                // copy the data
	pCtx->Unmap( pVertexBuffer, NULL );

	unsigned int stride = sizeof( PIXEL_VERTEX );
	unsigned int offset = 0;
	pCtx->IASetVertexBuffers( 0, 1, &pVertexBuffer, &stride, &offset );
	pCtx->IASetPrimitiveTopology( D3D11_PRIMITIVE_TOPOLOGY_POINTLIST );
	pCtx->Draw( size, 0 );
	pVertexBuffer->Release( );

	//delete[ ] pPix;

	this->pSwapchain->Present( 1, 0 );
}

bool GFX::SpawnWindow( )
{
	WNDCLASSA wc = { 0 };
	wc.lpfnWndProc = GFX::WindowHandler;
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpszClassName = "GaymeBoy";
	wc.hbrBackground = GetSysColorBrush( COLOR_BTNSHADOW );
	//wc.lpszMenuName = MAKEINTRESOURCEA( IDR_MENU1 );
	auto classerino = RegisterClassA( &wc );
	if ( !classerino )
	{
		Log( "Error registering class %d", GetLastError( ) );
		return false;
	}

	RECT desired_size = { 0, 0, this->wnd_w, this->wnd_h };
	AdjustWindowRect( &desired_size, WS_VISIBLE | WS_OVERLAPPEDWINDOW, FALSE );

	bool pleb_setup = false;
	this->hWindow = CreateWindowA( ( LPCSTR )classerino, "GaymeBoy", WS_VISIBLE | WS_OVERLAPPEDWINDOW, pleb_setup ? 0 : -1000, pleb_setup ? 0 : 250, desired_size.right - desired_size.left, desired_size.bottom - desired_size.top, 0, 0, 0, 0 );
	if ( !hWindow )
	{
		Log( "Error creating window %d", GetLastError( ) );
		return false;
	}

	// before we infinite loop, setup DX.
	if ( !GFX::InitializeDirectX( this->gb_w, this->gb_h ) ) // using gb dimensions so that it scales to our window size!
	{
		DestroyWindow( hWindow );
		UnregisterClassA( ( LPCSTR )classerino, 0 );
		return false;
	}

	this->ClearScreen( );

	this->initialized = true;

	MSG msg;
	while ( true )
	{
		if ( PeekMessage( &msg, 0, 0, 0, 0 ) )
		{
			if ( !GetMessage( &msg, 0, 0, 0 ) )
				break; // got a WM_QUIT

			TranslateMessage( &msg );
			DispatchMessage( &msg );
		}

		//this->RenderD3D( ); // possible thread timing issues here lmao
		std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
	}

	Log( "Window closed" );
	return true;
}

void GFX::ClearScreen( )
{
	D3DCOLORVALUE val = { 0 ,0 ,0, 1 }; // RGBA
	this->pCtx->ClearRenderTargetView( pRenderView, ( float* )&val );
	this->pSwapchain->Present( 1, 0 );
}

void GFX::DrawGPUBuffer( float** buf )
{
	this->CompileImage( buf );
}

GFX::GFX( int gb_w, int gb_h, int window_w, int window_h )
{
	this->gb_h = gb_h, this->gb_w = gb_w;
	if ( !window_h )
		this->wnd_h = gb_h;
	else
		this->wnd_h = window_h;

	if ( !window_w )
		this->wnd_w = gb_w;
	else
		this->wnd_w = window_w;

	// Create a window, setup pDevice, etc etc
	// Luckily for you, I know a decent amount of dx11
	// Then one must begin the message loop.
}