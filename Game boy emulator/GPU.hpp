#pragma once
#include "GenericHeader.h"
#include <vector>
#include "gfx/gfx.hpp"
#include <d3d9types.h>

enum class Color : uint8_t {
	White,
	LightGray,
	DarkGray,
	Black
};

	// stupid ass gameboy colors
struct Palette {
	union {
		uint8_t palette;
		struct {
			Color c0 : 2;
			Color c1 : 2;
			Color c2 : 2;
			Color c3 : 2;
		};
	};
};

class FrameBuffer {
	uint8_t* buf;
	float** shadow_buf; // real colors used by D3D (RGBA)

	int width = 0, height = 0;
public:
	FrameBuffer( int w, int h ) : buf( new uint8_t[ w*( h + 1 ) ] ), width( w ), height( h )
	{
		auto size = w * ( h + 1 );

		shadow_buf = new float*[ size ];
		memset( shadow_buf, 0, size * sizeof( float* ) );
	}

	void SetPixel( int x, int y, Color col )
	{
		this->buf[ ( y*width ) + x ] = ( uint8_t )col;
		auto loc = this->shadow_buf[ ( y*width ) + x ];
		if ( !loc )
			loc = this->shadow_buf[ ( y*width ) + x ] = new float[ 4 ];

		switch ( col )
		{
			case Color::White:
				loc[ 0 ] = 255 / 255.f;
				loc[ 1 ] = 255 / 255.f;
				loc[ 2 ] = 255 / 255.f;
				loc[ 3 ] = 255 / 255.f;
				break;
			case Color::LightGray:
				loc[ 0 ] = 170 / 255.f;
				loc[ 1 ] = 170 / 255.f;
				loc[ 2 ] = 170 / 255.f;
				loc[ 3 ] = 255 / 255.f;
				break;
			case Color::DarkGray:
				loc[ 0 ] = 85 / 255.f;
				loc[ 1 ] = 85 / 255.f;
				loc[ 2 ] = 85 / 255.f;
				loc[ 3 ] = 255 / 255.f;
				break;
			case Color::Black:
				loc[ 0 ] = 0 / 255.f;
				loc[ 1 ] = 0 / 255.f;
				loc[ 2 ] = 0 / 255.f;
				loc[ 3 ] = 255 / 255.f;
				break;
		}
	}

	void Clear( )
	{
		for ( int i = 0; i < width*height + 1; i++ )
			buf[ i ] = ( uint8_t )Color::White;
	}

	float** GetConvertedData( )
	{
		return this->shadow_buf;
	}

	uint8_t* GetData( )
	{
		return this->buf;
	}
};


class GameBoy;

// meant for use by the GameBoy
class GPU {
	GameBoy * pParent;
	char * pVRam;
	enum class GPUMode {
		HBLANK, // writing horizontal lines ?
		VBLANK, // switching to next row? idk.
		ACCESS_OAM, // access object attribute manager (or something, forget the name.)
		ACCESS_VRAM, // self explanatory
	} mode = GPUMode::ACCESS_OAM;


	const int CYCLES_PER_HBLANK = 204; // ripped from https://www.cl.cam.ac.uk/~pv273/slides/emulation.pdf
	const int CYCLES_PER_OAM = 80; // sprites
	const int CYCLES_PER_VRAM = 172; // background
	const int CYCLES_PER_SCANLINE = ( CYCLES_PER_OAM + CYCLES_PER_VRAM + CYCLES_PER_HBLANK ); // ???
	const int CYCLES_PER_VBLANK = 4560;
	//const uint8_t LINES_PER_FRAME = 144; // cause gb screen is like 160x144 or some shit

	int cycles_clocked = 0; // store number of cycles, as we can only do any of the above if our cycles are >= their CYCLES_PER
	// LCD Status
	
	LCDC* pLCDC = nullptr;
	LCDS* pLCDS = nullptr;
	Palette * pSpritePalette = nullptr; // array of 2.
	Palette * pBGPalette = nullptr;
	uint8_t* scroll_x = nullptr;
	uint8_t* scroll_y = nullptr;
	uint8_t* ly = nullptr; // register LY (Line-y)
	uint8_t* wy = nullptr; // Window-y
	uint8_t* wx = nullptr; // Window-x
	uint8_t* ly_compare = nullptr;
	InterruptFlag* interrupts = nullptr; // interrupt flag


	bool IsDisplayEnabled( );
	bool IsBGEnabled( );
	bool IsWindowEnabled( );
	bool UseFirstTileset( );
	bool UseFirstTilemap( );
	bool UseFirstWindowmap( );

	uint8_t GetPaletteColorForTilePos( uint16_t tile_address, uint8_t x, uint8_t y, uint8_t multiplier = 1 ); // bit of a mouth ful.
	Color GetColorForTilePos( uint16_t tile_address, uint8_t x, uint8_t y, Palette * pPalette );
	uint8_t GetLinePixel( uint8_t byte1, uint8_t byte2, uint8_t pixel_index ) const;
	Color GetColorFromPalette( uint8_t val, Palette * pPal );

	void DrawBGLine( );

	void DrawWindowLine( );

	void DrawSprites( );

	// calls above
	void DrawLines( );
	void InitializePointers( );

	bool initialized = false;

	const int bg_map_size = 256;
	FrameBuffer *buffer;
	void DrawBuffer( );
	GFX * pRenderer = nullptr;

public:

	GPU( char * pVRam, GameBoy * pParent, GFX* renderer );
	int Step( int cycles ); // does this return cycles? dunno.
};