#include "Timer.hpp"
#include "GBDefines.hpp"

Timer::Timer( MemoryArea_s * pMem )
{
	this->pMem = pMem;
	this->pModulo = &pMem->big_union.tma;
	this->pTimer = &pMem->big_union.tima;
	this->pTac = ( TAC* )&pMem->big_union.tac;
	this->pInterrupts = &pMem->big_union.interrupt_flag;
	this->time = this->clck.now( );
	this->pDivider = &pMem->big_union.div;
}

void Timer::Step( int cycles )
{
	this->div_cycles += cycles;
	if ( this->div_cycles >= ( Constants::GAMEBOY_CLOCK_SPEED / 16384 ) )
	{
		this->div_cycles = 0;
		( *this->pDivider )++;
	}


	if ( this->pTac->timer_start )
	{
		this->stored_cycles += cycles;
		int cycles_needed = 0;

		switch ( this->pTac->clock_select )
		{
			case 0:
				// 4096
				cycles_needed = ( Constants::GAMEBOY_CLOCK_SPEED / 4096 );
				break;
			case 1:
				//262144
				cycles_needed = ( Constants::GAMEBOY_CLOCK_SPEED / 262144 );
				break;
			case 2 :
				// 65536
				cycles_needed = ( Constants::GAMEBOY_CLOCK_SPEED / 65536 );
				break;
			case 3 :
				//16384
				cycles_needed = ( Constants::GAMEBOY_CLOCK_SPEED / 16384 );
				break;
			default:
				__debugbreak( ); // nani??
				break;
		}


		// if we dont meet the 'tick' requirement we gotta goooooooooooooo

		if ( stored_cycles >= cycles_needed )
		{
			stored_cycles = 0; // store remainder.
			auto old_timer = *this->pTimer;
			( *this->pTimer )++;
			if ( *this->pTimer < old_timer ) // overflowed
			{
				// trigger interrupt
				this->pInterrupts->timer = 1;
				// set timer to modulo?
				( *this->pTimer ) = *this->pModulo;
			}
		}
	}
}