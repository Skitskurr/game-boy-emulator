#include "GameBoy.hpp"
#include <thread>


HHOOK kbdHook = 0;
// WASD = 0->3, AB = 4->5
bool KeyStruct[ 8 ] = { 1, 1, 1, 1, 1, 1, 1, 1 };

GameBoy::GameBoy( int width, int height, GFX* renderer )
{
	//this->pRam = MemoryArea_s();
	this->pRam = new MemoryArea_s( );
	memset( this->pRam, 0, sizeof( this->pRam ) ); // that = {}; was triggering me
	Log( "RAM at %p", this->pRam );

	this->pCPU = new Z80( );
	this->pCPU->SetRam( this->GetRam( ) );
	this->width = width, this->height = height;
	this->pRenderer = renderer;
	this->pGPU = new GPU( this->pRam->big_union.Internals.VRam, this, this->pRenderer );
	this->pTimer = new Timer( this->pRam );
	this->pDebugger = new Debugger( this->pRam, this->pCPU );
	this->pAudio = new Audio( this );
}

GameBoy::~GameBoy( )
{ }

char * GameBoy::GetRam( )
{
	return this->pRam->big_union.full_memory;
}

void * GameBoy::GetPointer( uint16_t addr )
{
	return this->pCPU->GetRealAddress( addr );
}

uint8_t GameBoy::Read( uint16_t addr )
{
	return this->pCPU->Read( addr );
}

void GameBoy::Run( )
{
	if ( this->pCartridge == nullptr )
	{
		Log( "What are you, an idiot?" );
		return;
	}

	// step 4ever
	this->pDebugger->SetEnabled( false );
	this->pDebugger->SetVerbosityRange( 0, 0 );
	// should probably rate limit this, right?
	while ( this->Step( ) );
	Log( "Finished" );
	__debugbreak( );
}

void GameBoy::CheckInputs( )
{
	// TODO: Keyboard hook ty.

	// aids incominnngg // im saying aids a lot in this repo. must be because of the owner
	auto pGP = &this->pRam->big_union.game_pad;
	auto oldValue = *pGP;

	//pGP->reserved = pGP->reserved2 = 1;
	//pGP->p10 = pGP->p11 = pGP->p12 = pGP->p13 = 1;
	if ( !pGP->p14 ) // directional keys
	{
		pGP->p12 = KeyStruct[ 0 ]; // w ( or Select )
		pGP->p11 = KeyStruct[ 1 ]; // a
		pGP->p13 = KeyStruct[ 2 ]; // s ( or Start )
		pGP->p10 = KeyStruct[ 3 ]; // d 
	}

	if ( !pGP->p15 )
	{
		pGP->p10 = KeyStruct[ 4 ]; // A button
		pGP->p11 = KeyStruct[ 5 ]; // B button
		pGP->p13 = KeyStruct[ 6 ]; // Start
		pGP->p12 = KeyStruct[ 7 ]; // Select
	}

	if ( pGP->p15 && pGP->p14 )
		pGP->pins = 0xFF;
	else 
		if ( ( pGP->pins & 0xF ) < ( oldValue.pins & 0xF ) )
			this->pRam->big_union.interrupt_flag.pin_transition = 1;
}

int GameBoy::Step( )
{
	// alert debugger
	this->pDebugger->Notify( );

	auto cycles_spent = this->pCPU->Step( );
	// execute gpu
	this->pGPU->Step( cycles_spent );
	// execute timer
	this->pTimer->Step( cycles_spent );
	// check inputs
	this->CheckInputs( );
	// play sounds
	this->pAudio->Step( cycles_spent ); // may not need cycles but we shall seeeeeeeeeeeeee
	//if ( get_bit( this->pRam->big_union.nr_30, 7 ) )
	//{
	//	// Channel 3 (wave channel) enabled
	//	auto len = this->pRam->big_union.nr_31;
	//	auto output_level = ( this->pRam->big_union.nr_32 >> 3 ) & 3;
	//	if ( output_level != 0 ) // muted == 0
	//	{
	//		// 1 == 100% volume, 50% is 2, 25% is 3.
	//		this->pRenderer->PlayGBASound( this->pRam->big_union.wave_pattern, 32 /*always?*/ );

	//	}
	//}



	return cycles_spent; // return 0 to force an exit
}

bool GameBoy::LoadBIOS( const char * bios )
{
	std::ifstream fROM;
	fROM.open( bios, std::ifstream::binary | std::ios::ate );
	if ( !fROM || !fROM.is_open( ) || !fROM.good( ) )
	{
		Log( "Couldn't open the bios. Are you sure the name is right? (Don't forget the extension)" );
		return false;
	}
	size_t len = fROM.tellg( ); // tellg will otherwise cause len to be a std::streambuf
	fROM.seekg( 0 );

	//0x7FFF is the cartdrige size
	//but if the cartdrige has built in RAM it also uses from A000 to BFFF

	//TODO: Prepare for that situation

	/*
	Read never actually fails:
	it always returns *this
	*/
	fROM.read( pRam->big_union.full_memory, len );

	if ( fROM.fail( ) || fROM.bad( ) ) // thats what gets set instead
	{
		Log( "Not able to read the bios" );
		return false;
	}

	this->pCPU->SetRam( pRam->big_union.full_memory );
	this->biosLoaded = true;
	Log( "Bios loaded." );
	return true;
}

bool GameBoy::LoadROM( const char * rom )
{
	std::ifstream fROM;
	fROM.open( rom, std::ifstream::binary | std::ios::ate );
	if ( !fROM || !fROM.is_open( ) || !fROM.good( ) )
	{
		Log( "Couldn't open the rom. Are you sure the name is right? (Don't forget the extension)" );
		return false;
	}
	size_t len = fROM.tellg( ); // tellg will otherwise cause len to be a std::streambuf
	fROM.seekg( 0 );

	//0x7FFF is the cartdrige size
	//but if the cartdrige has built in RAM it also uses from A000 to BFFF

	//TODO: Prepare for that situation

	char * pRom = new char[ len + 1 ];
	fROM.read( pRom, len ); // seems they all contain their own bios?
	if ( fROM.fail( ) || fROM.bad( ) ) // thats what gets set instead
	{
		Log( "Not able to read the rom" );
		return false;
	}

	this->pCartridge = pRom;
	this->header = ( CartridgeHeader* )pRom;
	Log( "Cartridge type is %d", this->header->cart_type );
	this->pCPU->SetCartridge( pRom, this->header->cart_type );
	//this->pCPU->SetRam( pRam->big_union.full_memory );

	Log( "Got him" );
	return true;
}

void GameBoy::GetRomInfo( )
{
// TODO: this
//auto Cartheader = Rom.CartHeader;
//Log("Starting address at: %X\nTitle: %s\nLanguage:%s", Cartheader.startingAddress, Cartheader.Title, (Cartheader.language[0] /*?*/ == 0) ? "Japanese" : "English");
}

Z80 * GameBoy::GetCPU( )
{
	return this->pCPU;
}
