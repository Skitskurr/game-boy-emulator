#include "GameBoy.hpp"
#include "z80/Z80.hpp"
#include <thread>
#include <cassert>

//  11 (DAA), 1 (DAA)
#define TEST_NUMBER 1
#define TEST_MAX 12

void RunTests( GameBoy * pGB, bool all )
{
	if ( all )
		pGB->LoadROM( "gb_tests/cpu_instrs/cpu_instrs.gb" );
	else
	{
		char dir[ 0x250 ] = { 0 };
		GetCurrentDirectoryA( MAX_PATH, dir );
		strcat( dir, "\\gb_tests\\cpu_instrs\\individual\\*" );
		WIN32_FIND_DATAA fd = { 0 };

		HANDLE hFiles = FindFirstFileA( dir, &fd );
		assert( ( TEST_NUMBER < TEST_MAX ) && "Not enough files ya dingus." );
		assert( ( TEST_NUMBER > 0 ) && "I did this for ease of use ya dingus." );

		int chosen_index = TEST_NUMBER - 1;
		chosen_index += 2; // get a file based on index.// +1 because 0 is . and 1 is ..
		int i = 0;
		do
		{
			FindNextFileA( hFiles, &fd );
			i++;
		} while ( i < chosen_index );

		Log( "Loading %s", fd.cFileName );
		dir[ strlen( dir ) - 1 ] = 0;
		strcat( dir, fd.cFileName );
		pGB->LoadROM( dir );
	}

	pGB->Run( );
}

void StartGame( GFX* pRenderer )
{
	GameBoy* pGB = new GameBoy( 160, 144, pRenderer );

	pGB->LoadBIOS( "gb_bios.gb" );
	//RunTests( pGB, true );

	//pGB->LoadROM( "ffa.gb" );
	pGB->LoadROM( "pkmn_blue.gb" );
	//pGB->LoadROM( "tetris.gb" );
	//pGB->LoadROM( "sml.gb" );
	pGB->Run( );
}

int main() {
	GFX *pGFX = new GFX( 160, 144, 512, 512  );
	std::thread game_thread( StartGame, pGFX );
	game_thread.detach( );

	// TODO: Implement sound! (CPU or GPU? hmm.)

	// spawn window + permanently lock this thread.
	if ( !pGFX->SpawnWindow( ) )
		Log( "Window failed to launch" );
	else
		Log( "Hope you had fun :)" );

	// TODO: alert thread to stop doing loopy thingies.
	// TODO: Debugger

	getchar();
}