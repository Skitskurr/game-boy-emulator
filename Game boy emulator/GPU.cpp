#include "GPU.hpp"
#include "GBDefines.hpp"
#include "GameBoy.hpp"

bool GPU::IsDisplayEnabled( )
{
	return this->pLCDC->lcd_on;
}

bool GPU::IsBGEnabled( )
{
	return this->pLCDC->bg_wnd_on;
}

bool GPU::IsWindowEnabled( )
{
	return this->pLCDC->wnd_on;
}

bool GPU::UseFirstTileset( )
{
	return this->pLCDC->bg_wnd_data_select;
}

bool GPU::UseFirstTilemap( )
{
	return this->pLCDC->bg_map_select;
}

bool GPU::UseFirstWindowmap( )
{
	return this->pLCDC->wnd_map_select;
}

uint8_t GPU::GetPaletteColorForTilePos( uint16_t tile_address, uint8_t x, uint8_t y, uint8_t multiplier )
{
	uint8_t tile_x = x / Constants::TILE_WIDTH;
	uint8_t tile_y = y / ( Constants::TILE_HEIGHT * multiplier );

	// check if its a pixel that makes up a full 8x8 tile (i.e. instead of 0x0 this pixel is at location 4x4 in the tile  )
	// rather than a brand new tile
	uint8_t tile_pixel = x % Constants::TILE_WIDTH;
	uint8_t tile_line_index = y % ( Constants::TILE_HEIGHT * multiplier );

	uint8_t tile_line = tile_line_index * 2;// 2 ( bytes per line of pixels ) * #oflines
	uint16_t tile_line_start = tile_address + tile_line;

	uint8_t pix1 = this->pParent->Read( tile_line_start );
	uint8_t pix2 = this->pParent->Read( tile_line_start + 1 );

	// as game boy can use 2 different palettes with different numbers to denote colors, we have to convert.

	auto color = GetLinePixel( pix1, pix2, tile_pixel );
	return color;
}

Color GPU::GetColorForTilePos( uint16_t tile_address, uint8_t x, uint8_t y, Palette * pPalette )
{
	uint8_t tile_x = x / Constants::TILE_WIDTH;
	uint8_t tile_y = y / Constants::TILE_HEIGHT;

	// check if its a pixel that makes up a full 8x8 tile (i.e. instead of 0x0 this pixel is at location 4x4 in the tile  )
	// rather than a brand new tile
	uint8_t tile_pixel = x % Constants::TILE_WIDTH;
	uint8_t tile_line = y % Constants::TILE_HEIGHT;

	uint8_t index_into_tile = tile_line * 2;// 2 ( bytes per line of pixels ) * #oflines
	uint16_t tile_line_start = tile_address + index_into_tile;

	uint8_t pix1 = this->pParent->Read( tile_line_start );
	uint8_t pix2 = this->pParent->Read( tile_line_start + 1 );

	// as game boy can use 2 different palettes with different numbers to denote colors, we have to convert.

	auto color = GetColorFromPalette( GetLinePixel( pix1, pix2, tile_pixel ), pPalette );
	return color;
}

uint8_t GPU::GetLinePixel( uint8_t byte1, uint8_t byte2, uint8_t pixel_index ) const
{
	uint8_t b2 = get_bit( byte2, ( 7 - pixel_index ) );
	uint8_t b1 = get_bit( byte1, ( 7 - pixel_index ) );
	uint8_t ret = ( b2 << 1 ) | b1;
	return ret;
}

Color GPU::GetColorFromPalette( uint8_t val, Palette * pPal )
{
	switch ( val )
	{
		case 0:
			return pPal->c0;
			break;
		case 1:
			return pPal->c1;
			break;
		case 2:
			return pPal->c2;
			break;
		case 3:
			return pPal->c3;
			break;

		default:
			Log( "How???" );
			__debugbreak( );
			break;
	}

	return Color::White;
}

void GPU::DrawBGLine( )
{
	// use tileset 0
	bool use_ts_zero = this->UseFirstTileset( );
	// use tilemap 0
	bool use_tm_zero = !this->UseFirstTilemap( );

	// also known as tile data table
	uint16_t ts_loc = use_ts_zero ? Constants::tileset_zero_loc : Constants::tileset_one_loc;
	uint16_t tm_loc = use_tm_zero ? Constants::tilemap_zero_loc : Constants::tilemap_one_loc;

	auto scx = *this->scroll_x;
	auto scy = *this->scroll_y;
	auto ly = *this->ly;

	for ( int x = 0; x < this->pParent->width; x++ )
	{
		// what pixel it should be at in the buffer
		uint8_t pixel_x_in_map = ( scx + x ) % GPU::bg_map_size;
		uint8_t pixel_y_in_map = ( scy + ly ) % GPU::bg_map_size;
		// what tile the pixel would be at
		uint8_t tile_x = pixel_x_in_map / Constants::TILE_WIDTH;
		uint8_t tile_y = pixel_y_in_map / Constants::TILE_HEIGHT;

		// check if its a pixel that makes up a full 8x8 tile (i.e. instead of 0x0 this pixel is at location 4x4 in the tile  )
		// rather than a brand new tile
		uint8_t tile_pixel = pixel_x_in_map % Constants::TILE_WIDTH;
		uint8_t tile_line = pixel_y_in_map % Constants::TILE_HEIGHT;

		// tilemap = map of tiles to be drawn
		// tileset = what those tiles are

		// get tilemap index, this tells us what tile in the tilset to choose
		uint16_t index = ( tile_y * Constants::TILES_PER_LINE ) + tile_x;
		uint16_t tile_id_addr = tm_loc + index;
		// get tile number from said address
		uint8_t tid = this->pParent->Read( tile_id_addr );
		auto tile_offset = use_ts_zero ? tid * Constants::TILE_SIZE : ( ( int8_t )tid + 128 ) * Constants::TILE_SIZE;

		uint16_t tile_start = ts_loc + tile_offset;
		/*
			uint16_t tile_start = ts_loc + tile_offset;
			auto color = GetColorForTilePos( tile_start, pixel_x_in_map, pixel_y_in_map, pBGPalette );
		*/
		auto color = GetColorForTilePos( tile_start, pixel_x_in_map, pixel_y_in_map, pBGPalette );
		// as game boy can use 2 different palettes with different numbers to denote colors, we have to convert.
		buffer->SetPixel( x, ly, color );// get color from pixel
	}
}

void GPU::DrawWindowLine( )
{
	// TODO: Issue in here

	// use tileset 0
	bool use_ts_zero = this->UseFirstTileset( );
	// use tilemap 0
	bool use_windowmap_one = this->UseFirstWindowmap( );

	// also known as tile data table
	uint16_t ts_loc = use_ts_zero ? Constants::tileset_zero_loc : Constants::tileset_one_loc;
	uint16_t tm_loc = use_windowmap_one ? Constants::tilemap_one_loc : Constants::tilemap_zero_loc;

	auto cLine = *this->ly;
	auto wy = *this->wy;
	uint8_t pixel_y_in_map = cLine - wy;

	if ( cLine < wy )
		return;
	if ( wy < 0 || wy >= this->pParent->height )
		return;

	auto real_wx = ( *this->wx ) - 7;
	if ( real_wx < 0 || real_wx >= this->pParent->width )
		return;

	for ( int x = 0; x < this->pParent->width; x++ )
	{
		// what pixel it should be at in the buffer
		uint8_t pixel_x_in_map = ( x + real_wx );
		if ( pixel_x_in_map < 0 || pixel_x_in_map > this->pParent->width )
			continue;
		// what tile the pixel would be at
		uint8_t tile_x = pixel_x_in_map / Constants::TILE_WIDTH;
		uint8_t tile_y = pixel_y_in_map / Constants::TILE_HEIGHT;

		// check if its a pixel that makes up a full 8x8 tile (i.e. instead of 0x0 this pixel is at location 4x4 in the tile  )
		// rather than a brand new tile
		uint8_t tile_pixel = pixel_x_in_map % Constants::TILE_WIDTH;
		uint8_t tile_line = pixel_y_in_map % Constants::TILE_HEIGHT;

		// tilemap = map of tiles to be drawn
		// tileset = what those tiles are

		// get tilemap index, this tells us what tile in the tilset to choose
		uint16_t index = ( tile_y * Constants::TILES_PER_LINE ) + tile_x;
		uint16_t tile_id_addr = tm_loc + index;
		// todo: think tm_loc gets corrupted or smth
		// get tile number from said address
		uint8_t tid = this->pParent->Read( tile_id_addr );
		auto tile_offset = use_ts_zero ? tid * Constants::TILE_SIZE : ( ( int8_t )tid + 128 ) * Constants::TILE_SIZE;

		uint16_t tile_start = ts_loc + tile_offset;

		auto color = GetColorForTilePos( tile_start, pixel_x_in_map, pixel_y_in_map, pBGPalette );

		buffer->SetPixel( x, cLine, color );// get color from pixel
	}
}

void GPU::DrawSprites( )
{
	// only 10 sprites can appear on one line, but im not tracking that just yet.
	auto pSprites = ( SpriteAttribute* )this->pParent->GetPointer( Constants::sprite_attribute_loc );
	// use tileset 0
	bool use_ts_zero = this->UseFirstTileset( );
	// use tilemap 0
	bool use_tm_zero = !this->UseFirstTilemap( );
	int tile_size_mul = this->pLCDC->obj_size_sym ? 2 : 1;
	// if PLCDC->Bit2 is 1 then sprites are now 8x16 rather than 8x8

	for ( int i = 0; i < 40; i++ )
	{
		auto curSprite = pSprites[ i ];

		if ( !curSprite.x || !curSprite.y )
			continue;
		if ( curSprite.x >= 168 || curSprite.y >= 160 )
			continue;

		auto pPalette = &this->pSpritePalette[ curSprite.sprite_attributes.palette_num ];
		uint16_t tile_offset = curSprite.tile_number * Constants::TILE_SIZE;
		uint16_t tile_start = Constants::tileset_zero_loc + tile_offset;
		uint8_t real_x = curSprite.x - 8;  // these can be negative unfortunately.
		uint8_t real_y = curSprite.y - 16;	// these can be negative unfortunately.

		for ( int y = 0; y < Constants::TILE_HEIGHT * tile_size_mul; y++ )
		{
			for ( int x = 0; x < Constants::TILE_WIDTH; x++ )
			{
				auto drawX = real_x + x;
				auto drawY = real_y + y;

				if ( drawX < 0 || drawX > this->pParent->width )
					continue;
				if ( drawY < 0 || drawY > this->pParent->height )
					continue;
					// check for flipped.
				auto tile_x = curSprite.sprite_attributes.x_flip ? ( Constants::TILE_WIDTH - x - 1 ) : x;  // read from right side if flipped 
				auto tile_y = curSprite.sprite_attributes.y_flip ? ( ( Constants::TILE_HEIGHT * tile_size_mul ) - y - 1 ) : y; // read from bottom if flipped

				// get pixel from tile.
				auto color = GetPaletteColorForTilePos( tile_start, tile_x, tile_y, tile_size_mul );
				if ( color == 0 ) // transparent apparently.
					continue;

				auto draw_color = GetColorFromPalette( color, pPalette );

				if ( curSprite.sprite_attributes.priority == 1 )
				{
					// dont draw if buffer->paletteColor > 0
				}

				buffer->SetPixel( drawX, drawY, draw_color );
			}
		}
	}
}

void GPU::DrawLines( )
{
	// check if display enabled
	if ( this->IsDisplayEnabled( ) == false )
		return;

	// check if bg enabled, if so, draw bg
	if ( this->IsBGEnabled( ) )
		DrawBGLine( );

	// check if window enabled, if so, draw window
	if ( this->IsWindowEnabled( ) )
		this->DrawWindowLine( );
}

void GPU::InitializePointers( )
{
	MemoryArea_s* pMem = ( MemoryArea_s* )this->pParent->GetRam( );

	// do i really need these ifs?

	this->pLCDS = ( decltype( pLCDS ) )&pMem->big_union.stat;
	this->pLCDC = ( decltype( pLCDC ) )&pMem->big_union.lcdc;
	this->pSpritePalette = ( decltype( pSpritePalette ) )&pMem->big_union.obp0;
	this->pBGPalette = ( decltype( pBGPalette ) )&pMem->big_union.bgp;
	this->scroll_x = &pMem->big_union.scx;
	this->scroll_y = &pMem->big_union.scy;
	this->ly = &pMem->big_union.ly;
	this->interrupts = ( InterruptFlag* )&pMem->big_union.interrupt_flag;
	this->ly_compare = &pMem->big_union.lyc;
	this->wy = &pMem->big_union.wy;
	this->wx = &pMem->big_union.wx;

// do for anything else we may need

	this->initialized = true;
}

GPU::GPU( char * pVRam, GameBoy * pParent, GFX* renderer )
{
	this->pRenderer = renderer;
	this->pParent = pParent;
	this->buffer = new FrameBuffer( this->pParent->width, this->pParent->height );
	this->pVRam = pVRam;
}

int GPU::Step( int cycles )
{
	if ( !this->initialized )
		this->InitializePointers( );

	if ( this->pParent->GetCPU( )->IsStopped( ) )
		return 0;

	this->cycles_clocked += cycles;

	switch ( this->mode )
	{
		case GPUMode::ACCESS_OAM:
			if ( this->cycles_clocked >= CYCLES_PER_OAM )
			{
				// had no clue this was even an operator tbh
				this->cycles_clocked %= CYCLES_PER_OAM; // so we dont discard clocks if say this is 81, we want to save that 1 for future operations as OAM access only requires 80
				// when disallowing the CPU from transfering data to the OAM+RAM, you set bits 1 and 0 of the LCDStatus register to 1 (GB docs page 52)
				this->pLCDS->mode = LCDSMode::transfer; // set first 2 bits breehhh
				// now we access VRAM
				this->mode = GPUMode::ACCESS_VRAM;
			}
			break;
		case GPUMode::ACCESS_VRAM: // kind of like end of scanline apparently?
			if ( this->cycles_clocked >= CYCLES_PER_VRAM )
			{
				this->cycles_clocked %= CYCLES_PER_VRAM;

				bool hblank_interrupt = this->pLCDS->interrupt_mode_00;
				if ( hblank_interrupt )
					this->interrupts->lcdc = 1;
					// set cpu's interrupt flag's bit 1 to 1 (video.cc from jgilchrist's github)

				bool ly_interrupt = this->pLCDS->interrupt_lyc_conincidence;
				bool ly_coin = ( *this->ly_compare == *this->ly ); // compare LY with our current line.  if they match + we're interrupting, then set interrupt flag.
				if ( ly_interrupt && ly_coin )
					this->interrupts->lcdc = 1;
				// set LCDS bit 2 to ly_coin, as thats the ly_coincidence bit
				this->pLCDS->coincidence = ly_coin;

				//  no longer accessing VRAM or OAM, so we can free bit 1 and 0.
				this->pLCDS->mode = LCDSMode::hblank; // can just set those 2 specifically, but i think we can also just 0 it out.

				// next is HBLANK
				this->mode = GPUMode::HBLANK;
			}
			break;
		case GPUMode::HBLANK:
			if ( this->cycles_clocked >= CYCLES_PER_HBLANK )
			{
				this->cycles_clocked %= CYCLES_PER_HBLANK;
				// set lines in buffer
				this->DrawLines( );
				// increment our line
				( *this->ly )++;

				// after 144 we start VBlanking (Page 55, LY)
				//this->current_line++;
				if ( *ly == 144 )
				{
					this->mode = GPUMode::VBLANK;
					this->pLCDS->mode = LCDSMode::vblank;
					this->interrupts->vblank = true;
				}
				else
				{
					// draw next line, need information
					this->pLCDS->mode = LCDSMode::search;
					this->mode = GPUMode::ACCESS_OAM;

					// perhaps they are enver able to access_oam because of my bad timing?
				}
			}
			break;
		case GPUMode::VBLANK:
			if ( this->cycles_clocked >= CYCLES_PER_SCANLINE )
			{
				this->cycles_clocked %= CYCLES_PER_SCANLINE;
				( *this->ly )++;
				if ( *this->ly >= 153 )
				{
					// lastly, draw sprites on top of everything
					this->DrawSprites( );
					// now draw the actual buffer.
					this->DrawBuffer( );
					( *this->ly ) = 0;
					this->buffer->Clear( );
					// this->buffer = 0 but idk what buffer is
					this->mode = GPUMode::ACCESS_OAM;
					this->pLCDS->mode = LCDSMode::search;
				}
			}
			break;
	}
	return 0;
}

void GPU::DrawBuffer( )
{
	// clear screen
	// set the ol' pixelerinos
	// load texture from image?!?!?!?!?!?!?!?!?!?!?!?!??!?!
	// set sprites to specific texture

	// tell DX what pixels to set to what color i suppose ??
	this->pRenderer->DrawGPUBuffer( this->buffer->GetConvertedData( ) );
}
