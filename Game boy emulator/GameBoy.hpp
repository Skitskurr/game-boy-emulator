#pragma once
#include "z80/Z80.hpp"
#include "GPU.hpp"
#include "gfx/gfx.hpp"
#include "Timer.hpp"
#include "Debugger.hpp"
#include "audio.hpp"

class GameBoy
{
	bool biosLoaded = false;
	struct CartridgeHeader {
		char _pad[ 0x134 ];
		char title[ 0xF ]; // 134
		bool cgb_flag; // 143
		char licensee_code[ 2 ]; // 144
		bool sgb_flag; // 146
		CartridgeType cart_type; // 147
	}* header; //size 0x50

	MemoryArea_s *pRam; // NO PUBLIC. BAD. Nothing should be able to touch the internals of your class (i.e. mem_a->InternalRam) because shit is subject to change,
	char * pCartridge = nullptr;
	//Consider using Getters instead

	Z80 *pCPU;
	GPU *pGPU;
	GFX* pRenderer; // actual rendering (DX11)
	Timer * pTimer;
	Debugger * pDebugger;
	Audio * pAudio;

	const int GAMEBOY_CLOCK_SPEED = 4194304; // Clocks / Second
	std::chrono::time_point < std::chrono::steady_clock > curTime;
public:
	int width, height;
	GameBoy(int width, int height, GFX* renderer);
	~GameBoy();

	char * GetRam( );

	// infinite steps
	void* GetPointer( uint16_t addr );
	uint8_t Read( uint16_t addr );
	void Run( );
	void CheckInputs( );
	int Step( );
	bool LoadBIOS( const char* ); // optional, but BIOS does rom checking for us.
	bool LoadROM(const char *);
	void GetRomInfo( ); //Just 4Fun
	Z80* GetCPU( );
};

