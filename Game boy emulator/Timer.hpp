#pragma once
#include "GenericHeader.h"
#include <chrono>

class Timer {
	int frequency = 4096; //  16384, 65536 262144 hz
	MemoryArea_s * pMem;
	uint8_t * pModulo;  // when TIMA overflows, this is loaded into it.
	uint8_t * pTimer;	// incremented at the rate that TAC ( 0xFF07 ) specifies.
	uint8_t * pDivider; // just incremented 16384 times/second
	union TAC {
		uint8_t full;
		struct {
			uint8_t clock_select : 2; // 0. 00 = 4096, 1 = 262144, 2 = 65536, 3 = 16384
			uint8_t timer_start : 1; // 2
		};
	};
	TAC * pTac; // ff07
	InterruptFlag * pInterrupts;

	std::chrono::high_resolution_clock clck;
	std::chrono::time_point<std::chrono::steady_clock> time;

	int stored_cycles = 0;
	int div_cycles = 0;
public:
	Timer( MemoryArea_s * pMem );
	void Step( int cycles );
};