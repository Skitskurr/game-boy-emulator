#include "Debugger.hpp"

char * Debugger::GetLog( CPUState * pState )
{
	return this->GetLog( &pState->regs, pState->op1, pState->op2 );
}

char * Debugger::GetLog( CPURegisters * pRegisters, uint8_t op1, uint8_t op2 )
{
	auto oldpc = pRegisters->pc;
	char write_buffer[ 0x100 ] = { 0 };
	sprintf_s( write_buffer, "Loc: %04X ( %04X ) -- %02X", oldpc, ( uintptr_t )this->pCPU->GetRealAddress( oldpc ) - (uintptr_t)this->pCPU->GetCartridge() , op1 );
	if ( op1 == 0xCB )
		sprintf_s( write_buffer, "%s %02X\n", write_buffer, op2 );
	else
		sprintf_s( write_buffer, "%s\n", write_buffer );

	sprintf_s( write_buffer, "%sSP : %04X\n", write_buffer, pRegisters->sp );
	auto pFlags = pRegisters->GetFlags( );
	sprintf_s( write_buffer, "%sAF : %04X -- %d %d %d %d\n", write_buffer, pRegisters->af.full, pFlags->z, pFlags->n, pFlags->h, pFlags->c );
	sprintf_s( write_buffer, "%sBC : %04X\n", write_buffer, pRegisters->gp.bc.full );
	sprintf_s( write_buffer, "%sDE : %04X\n", write_buffer, pRegisters->gp.de.full );
	sprintf_s( write_buffer, "%sHL : %04X\n", write_buffer, pRegisters->gp.hl.full );

	char * outBuffer = new char[ 0x100 ];
	strcpy_s( outBuffer, 0x100, ( char* )write_buffer );
	return outBuffer;
}

void Debugger::Dump( int starting_step, DumpType type )
{
	log_file << "--------------------------------------- BEGINNING OF DUMP ---------------------------------------" << std::endl;

	for ( auto &x : this->pStates )
	{
		if ( x->step >= starting_step )
		{
			if ( ( type == DumpType::has_changes && x->has_changes ) || ( type == DumpType::no_changes && !x->has_changes ) || type == DumpType::indiscriminate || x->step == starting_step )
			{
				// always print first step so we know our baseline.
				auto log = this->GetLog( x );
				log_file << log << std::endl;
				delete log;
			}
		}
	}

	log_file << "--------------------------------------- END OF DUMP ---------------------------------------" << std::endl;
}

void Debugger::SetVerbosityRange( uint16_t lo, uint16_t hi )
{
	this->highRange = hi;
	this->lowRange = lo;
	this->verbose = true;
}

// using oldpc incase of a jump, so we dont erroneously print the wrong thing.
std::vector< uint16_t > loop_check;
bool Debugger::DumpRegisters( )
{
	if ( !this->verbose )
		return false;

	auto pRegisters = this->pCPU->GetRegisters( );
	auto oldpc = pRegisters->pc;
	if ( oldpc >= this->lowRange && ( oldpc <= this->highRange || !this->highRange ) && !this->pCPU->IsDMGEnabled( ) )
	{
		// print opcodes rather than addresses
		bool opcodes = false;
		auto op1 = this->pCPU->Read( oldpc );
		auto op2 = this->pCPU->Read( oldpc + 1 );

		if ( std::find( loop_check.begin( ), loop_check.end( ), opcodes ? op1 : oldpc ) != loop_check.end( ) )
			return false;
		loop_check.push_back( opcodes ? op1 : oldpc ); // hopefully not too intensive.

													  // Log to file instead because its quicker than dumping to console
		auto toWrite = GetLog( pRegisters, op1, op2 );
		log_file << toWrite << std::endl;
		delete toWrite;
		// new opcode
		return true;
	}

	return false;
}

Debugger::Debugger( MemoryArea_s * pRam, Z80 * cpu )
{

	this->pCPU = cpu;
	this->pMemory = pRam;
}

int stored_hit = 0;
void Debugger::Notify( )
{
	//if ( this->pCPU->IsHalted( ) )
	//	__debugbreak( );

	if ( enabled )
	{
		DumpRegisters( );
		if ( this->log_file.is_open( ) == false )
			this->log_file.open( "gayme_log.log", std::ios::trunc );

		// get old state for comparison
		CPUState * pLast = nullptr;
		if ( this->pStates.size( ) )
			pLast = this->pStates.back( );

		// get new state.
		auto pRegs = this->pCPU->GetRegisters( );
		CPUState * pState = new CPUState( pRegs, pLast );
		pState->op1 = this->pCPU->Read( pRegs->pc );
		if ( pState->op1 == 0xCB )
			pState->op2 = this->pCPU->Read( pRegs->pc + 1 );

		pState->step = this->current_step++;
		// store.
		this->pStates.push_back( pState );
		// now can do stuff like breakpoints etc etc etc
	}

	if ( this->pMemory->big_union.sc == 0x81 )
	{
		printf_s( "%c", this->pMemory->big_union.sb );
		this->pMemory->big_union.sc = 0;
	}
}

void Debugger::SetEnabled( bool b )
{
	this->enabled = b;
}

bool Debugger::CPUState::operator==( const CPUState & other )
{
	// anything except PC really.
	if ( this->regs.af.full != other.regs.af.full )
		return false;
	if ( this->regs.gp.bc.full != other.regs.gp.bc.full )
		return false;
	if ( this->regs.gp.de.full != other.regs.gp.de.full )
		return false;
	if ( this->regs.gp.hl.full != other.regs.gp.hl.full )
		return false;
	if ( this->regs.sp != other.regs.sp )
		return false;

	return true;
}

Debugger::CPUState::CPUState( CPURegisters * pRegs, CPUState * pLastState )
{
	memcpy( &this->regs, pRegs, sizeof( this->regs ) );
	if ( pLastState )
		this->has_changes = !( *this == *pLastState );
}