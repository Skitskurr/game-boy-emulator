#pragma once
#include "GenericHeader.h"

namespace Constants {
	const uint16_t tileset_zero_loc = 0x8000;
	const uint16_t tileset_one_loc = 0x8800;

	const uint16_t tilemap_zero_loc = 0x9800;
	const uint16_t tilemap_one_loc = 0x9C00;

	const uint16_t sprite_attribute_loc = 0xFE00; // to 0xFE9F (so 40 entries ( 4 bytes each )) 

	const uint8_t TILE_WIDTH = 8;
	const uint8_t TILE_HEIGHT = 8;
	const uint8_t TILES_PER_LINE = 32;

	// tiles create 8x8 images, but only consist of 16 bytes. ( 8 lines of 2 bytes each )
	const uint8_t TILE_SIZE = 8 * 2;
	const int GAMEBOY_CLOCK_SPEED = 4194304; // Clocks / Second
};
