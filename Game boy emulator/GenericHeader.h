#pragma once
#include <Windows.h>
#include <iostream>
#include <fstream>
//Rip Chip8

#define DBG 1

#if DBG
// sleeker Log fn
#define Log(format, ...) printf_s("[ %-20s ] " ## format ## "\n", __FUNCTION__, __VA_ARGS__ )
#else
#define Log //
#endif

#define test_bit(val, bit) ( (val & ( 1 << bit)) >> bit)
#define set_bit(val, bit, toset) ( val = toset ? val | ( 1 << bit ) : val & ~( 1 << bit ) )
#define get_bit(val, bit) ( (val >> bit ) & 1 )


union InterruptFlag {
	uint8_t interrupts;
	struct {
		uint8_t vblank : 1;
		uint8_t lcdc : 1;
		uint8_t timer : 1;
		uint8_t serial_io_done : 1;
		uint8_t pin_transition : 1;
	};
};

union GamePad {
	uint8_t pins;
	struct {
		uint8_t p10 : 1; // Right or A
		uint8_t p11 : 1; // Left or B
		uint8_t p12 : 1; // UP
		uint8_t p13 : 1; // DOWN
		uint8_t p14 : 1; // Select DIRs
		uint8_t p15 : 1; // Select Buttons
		uint8_t reserved : 1; // always 1
		uint8_t reserved2 : 1; // always 1
	};
};

enum class LCDSMode : uint8_t {
	hblank,
	vblank,
	search,
	transfer,
};

union LCDS {
	uint8_t status;
	struct {
		LCDSMode mode : 2; // 0 = hblank, 1 = vblank, 2 = searching(no oam access), 3 = transfering
		uint8_t coincidence : 1; // LYC = LCDC LY == 1, else 0
		uint8_t interrupt_mode_00 : 1; // interrupt on mode == 00 if this is set to 1
		uint8_t interrupt_mode_01 : 1; // likewise
		uint8_t interrupt_mode_10 : 1; // likewise
		uint8_t interrupt_lyc_conincidence : 1; // likewise
	};
};

// LCD Contrl
union LCDC {
	uint8_t control;
	struct {
		uint8_t bg_wnd_on : 1;
		uint8_t obj_display_on : 1;
		uint8_t obj_size_sym : 1; // 0 = 8*8 sprites, else 8*16 sprites (w/h)
		uint8_t bg_map_select : 1; // 0 = 98FF->9BFF else 9C->9F
		uint8_t bg_wnd_data_select : 1; // 0 = 88->97 : 80 -> 8F -- bg + window tile data select
		uint8_t wnd_on : 1;
		uint8_t wnd_map_select : 1; // 7 // 0 = 98->9B : 9C->9F
		uint8_t lcd_on : 1;
	};
};

union SpriteAttribute {
	uint32_t full;
	struct {
		uint8_t y; // Sprite y pos - 16 ( value of 0 or >= 160 will hide it from the screen as its offscreen )
		uint8_t x; // Sprite x pos - 8 ( >= 168 || == 0 = off screen )
		uint8_t tile_number; // tile number, selects from 8k -> 8FFF

		union {
			uint8_t attributes;
			struct {
				uint8_t cgb_palette_num : 3;
				uint8_t tile_vram_bank : 1; // 3
				uint8_t palette_num : 1; // 4 ( for non-cgb mode. 0 = OBP0, 1 = OBP1 )
				uint8_t x_flip : 1; // 5
				uint8_t y_flip : 1; // 6
				uint8_t priority : 1; // 0 = above bg, 1 == behind bg colors 1->3
			};
		} sprite_attributes;
	};
};

struct MemoryArea_s {
	union MemUnion {
		char full_memory[ 0x10000 + 0x100/*for BIOS*/ ]; // this is a full 64 kb.

		// temporary (?)
		struct {
			char _pad[ 0xFF00 ];
			GamePad game_pad; // 0xFF00 -- pad 1
			uint8_t sb; // 0xFF01 -- serial transfer data
			uint8_t sc; // 0xFF02 -- SIO control
			uint8_t _pad2; // 0xFF03
			uint8_t div;// 0xff04 -- Divider register (incremented 16384 times a second apparently??)
			uint8_t tima;  //0xff05 timer counter
			uint8_t tma;   //0xff06 timer modulo
			uint8_t tac;   //0xff07 timer control
			char _pad3[ 0xFF0F - 0xFF08 ]; // 0xff08
			InterruptFlag interrupt_flag;	// 0xff0f -- which interrupt we want to fire
			uint8_t nr_10; // 0xFF10 -- sound mode 1 register
			uint8_t nr_11; // 0xFF11 -- sound mode 1 register too lazy to change commenet
			uint8_t nr_12; // 0xFF12 -- sound mode 1 register too lazy to change commenet
			uint8_t nr_13; // 0xFF13 -- sound mode 1 register too lazy to change commenet
			uint8_t nr_14; // 0xFF14 -- sound mode 1 register too lazy to change commenet
			uint8_t _pad4; // 0xFF15
			uint8_t nr_21; // 0xFF16 -- sound mode 2 register
			uint8_t nr_22; // 0xFF17 -- sound mode 2 register
			uint8_t nr_23; // 0xFF18 -- sound mode 2 register
			uint8_t nr_24; // 0xFF19 -- sound mode 2 register
			uint8_t nr_30; // 0xFF1A -- sound mode 3 register
			uint8_t nr_31; // 0xFF1B -- sound mode 3 register
			uint8_t nr_32; // 0xFF1C -- sound mode 3 register
			uint8_t nr_33; // 0xFF1D -- sound mode 3 register
			uint8_t nr_34; // 0xFF1E -- sound mode 3 register
			uint8_t _pad5; // 0xFF1F
			uint8_t nr_41; // 0xFF20 -- sound mode 4 register
			uint8_t nr_42; // 0xFF21 -- sound mode 4 register
			uint8_t nr_43; // 0xFF22 -- sound mode 4 register
			uint8_t nr_44; // 0xFF23 -- sound mode 4 register
			uint8_t nr_50; // 0xFF24 -- sound mode 5 register
			uint8_t nr_51; // 0xFF25 -- sound mode 5 register
			uint8_t nr_52; // 0xFF26 -- sound mode 5 register
			char _pad6[ 0xFF30 - 0xFF27 ]; // 0xFF27
			uint8_t wave_pattern[ 0x10 ]; // 0xFF30 -- Wave pattern RAM (sound data)
			uint8_t lcdc;	// 0xFF40 -- LCD Control
			uint8_t stat;	// 0xFF41 -- LCDC Status
			uint8_t scy;	// 0xFF42 -- Scroll Y
			uint8_t scx;	// 0xFF43 -- Scroll X
			uint8_t ly;		// 0xFF44 -- LCDC Y-Coord
			uint8_t lyc;	// 0xFF45 -- LY Compare
			uint8_t dma;	// 0xFF46 -- DMA Transfer + Start addr
			uint8_t bgp;	// 0xFF47 -- BG + Window Palette Data
			uint8_t obp0;	// 0xFF48 -- object palette 0 data
			uint8_t obp1;	// 0xFF49 -- object palette 1 data
			uint8_t wy;		// 0xFF4A -- window y pos
			uint8_t wx;		// 0xFF4B -- window x pos
			char _pad7[ 0xFFFF - 0xFF4C ]; // high-ram
			uint8_t interrupts_enabled;		// 0xFFFF -- interrupt enable flag ( which interrupts are allowed to fire )
		};


		struct InternalMemory {
			struct ROMSpace_s {
				char bank0[ 0x4000 ];
				char switchBanks[ 0x4000 ];
			}RomSpace;

			char VRam[ 0x2000 ]; // v for Video fam

			char switchable_ram[ 0x2000 ];
			char internal_ram[ 0x2000 ];
			char internal_ram_echo[ 0x1E00 ];
			char oam_memory[ 0xA0 ];
			char the_void[ 0x60 ];
			char io_shit[ 0x4C ];
			char the_abyss[ 0x34 ];
			char internal_ram_2[ 0x7f ];
			char interrupt_enable;
			//const int test = sizeof( InternalMemory );

			//struct BGMap_s {
			//	char data1[ 0x400 ];
			//	char data2[ 0x400 ];
			//}BGMap;//size 0x800

			//char cartRAM[ 0x2000 ]; //If available

			//struct InternalRam_s {
			//	char bank0[ 0x1000 ];
			//	char bank1_7[ 0x1000 ]; // 1_7? why idk didnt read docs yet
			//}InternalRam;//size 0x2000

			//char reserved[ 0x1E00 ]; //may be altered or unavailable in future versions of Windows LUL
			//char mightUse[ 0x200 ];
		}Internals;

	} big_union;
};

enum class CartridgeType : uint8_t {
	ROM_ONLY = 0, // no switchbank
	MBC1, // switch bank
	MBC1_RAM, // switch bank + ram
	MBC1_RAM_BATTERY,
	MBC2 = 5,
	MBC2_BATTERY, // FFA
	ROM_RAM = 8,
	ROM_RAM_BATTERY,
	MMM01 = 0xB,
	MMM01_RAM,
	MBC3_RAM_BATTERY = 0x13, // PKMN uses this so im skipping everything lmao
};

extern HHOOK kbdHook;
// WASD = 0->3, AB = 4->5
extern bool KeyStruct[ 8 ];
