#pragma once
#include "GenericHeader.h"
#include "z80/Z80.hpp"
#include <fstream>
#include <vector>

// GOAL: Be able to provide an accurate log of register and memory changes at each instruction.

class Debugger {

	struct CPUState {
		// Contain Registers (all)
		union {
			uint16_t full_instr;
			struct {
				uint8_t op1;
				uint8_t op2;
			};
		};

		int step; // what step were we on when we executed this instruction.

		CPURegisters regs; 
		// a delta of memory state, however we'll skip that for now.
		bool has_changes;
		bool operator==( const CPUState& other );
	public:
		CPUState( CPURegisters * pRegs, CPUState* pLastState);
	};

	std::vector<CPUState*> pStates;

	bool enabled = false;

	Z80* pCPU = nullptr;
	MemoryArea_s* pMemory = nullptr;
	int lowRange = 0, highRange = 0;
	bool DumpRegisters( );
	bool verbose = false;
	std::ofstream log_file;
	int current_step = 0;

	char * GetLog( CPUState * pState );
	char * GetLog( CPURegisters * pRegisters, uint8_t op1 = 0, uint8_t op2 = 0 );
	enum class DumpType {
		indiscriminate,
		has_changes,
		no_changes,

	};
	void Dump( int starting_step, DumpType type ); // dump the pStates vector from starting_step to current_step
public:
	// dont call if you dont want logging.
	void SetVerbosityRange( uint16_t lo, uint16_t hi );
	Debugger( MemoryArea_s* pRam, Z80* cpu );
	// CPU is about to take a step.
	void Notify( );
	void SetEnabled( bool b );
};