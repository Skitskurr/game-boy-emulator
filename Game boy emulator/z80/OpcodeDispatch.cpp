#include "Z80.hpp"

// OR this with base opcode to see what the source is
enum class SRCOperand {
	B = 0,
	C,
	D,
	E,
	H,
	L,
	HL,
	A, // cause why not ok Z80
};

enum class DSTOperand {
	B = 0x0,
	C = 8,
	D = 0x10,
	E = 0x18,
	H = 0x20,
	L = 0x28,
	//S = 0x30,
	HL = 0x30,
	A = 0x38,
};

// aids name
enum class AsPairs {
	BC = 0,
	DE = 0x10,
	HL = 0x20,
	AF = 0x30,
	SP = 0xF8, // ?
};

enum class BranchCondition {
	UNCONDITIONAL = 1,
	NZ = 0,
	Z = 8,
	NC = 0x10,
	C = 0x16,	// 18 for JR because fuck logic
	PO = 0x20,	// parity odd
	PE = 0x28,	// parity even
	NS = 0x30,	// positive sign
	S = 0x38,	// negative sign
	MAX
};

std::pair< uint8_t*, uint8_t*> Z80::GetOperandsByIndex( int dst, int src, bool as_pair )
{
	// matches above indexes.
	void * dstReg = 0, *srcReg = 0;
	if ( !as_pair )
	{
		switch ( ( DSTOperand )dst )
		{
			case DSTOperand::B:
				dstReg = &this->Registers.gp.bc.first;
				break;
			case DSTOperand::C:
				dstReg = &this->Registers.gp.bc.second;
				break;
			case DSTOperand::D:
				dstReg = &this->Registers.gp.de.first;
				break;
			case DSTOperand::E:
				dstReg = &this->Registers.gp.de.second;
				break;
			case DSTOperand::H:
				dstReg = &this->Registers.gp.hl.first;
				break;
			case DSTOperand::L:
				dstReg = &this->Registers.gp.hl.second;
				break;
			case DSTOperand::A:
				dstReg = &this->Registers.af.first;
				break;
			case DSTOperand::HL: // [HL]
				dstReg = &this->Registers.gp.hl.full;
				break;
		}

		switch ( ( SRCOperand )src )
		{
			case SRCOperand::B:
				srcReg = &this->Registers.gp.bc.first;
				break;
			case SRCOperand::C:
				srcReg = &this->Registers.gp.bc.second;
				break;
			case SRCOperand::D:
				srcReg = &this->Registers.gp.de.first;
				break;
			case SRCOperand::E:
				srcReg = &this->Registers.gp.de.second;
				break;
			case SRCOperand::H:
				srcReg = &this->Registers.gp.hl.first;
				break;
			case SRCOperand::L:
				srcReg = &this->Registers.gp.hl.second;
				break;
			case SRCOperand::HL:
				srcReg = &this->Registers.gp.hl.full;
				break;
			case SRCOperand::A:
				srcReg = &this->Registers.af.first;
				break;
		}
	}
	else
	{
		switch ( ( AsPairs )dst )
		{
			case AsPairs::BC:
				dstReg = &this->Registers.gp.bc.full;
				break;
			case AsPairs::DE:
				dstReg = &this->Registers.gp.de.full;
				break;
			case AsPairs::HL:
				dstReg = &this->Registers.gp.hl.full;
				break;
			case AsPairs::AF:
				dstReg = &this->Registers.af.full;
				break;
			case AsPairs::SP:
				dstReg = &this->Registers.sp;
				break;
		}

		switch ( ( AsPairs )src )
		{
			case AsPairs::BC:
				srcReg = &this->Registers.gp.bc.full;
				break;
			case AsPairs::DE:
				srcReg = &this->Registers.gp.de.full;
				break;
			case AsPairs::HL:
				srcReg = &this->Registers.gp.hl.full;
				break;
			case AsPairs::AF:
				srcReg = &this->Registers.af.full;
				break;
		}
	}


	return { ( uint8_t* )dstReg, ( uint8_t* )srcReg };
}


int Z80::HandleMisc( uint8_t base_opcode, uint8_t opcode )
{
	auto as_pairs = GetOperandsByIndex( 0, 0, 0 ); // for auto cause im lazy.
	auto pFlags = this->Registers.GetFlags( );

	switch ( opcode )
	{
		default:
			__debugbreak( );
			break;

		case 0x76:
			// HALT.
			// do nothing until interrupt.
			// if interrupts are disabled, HALT will actually perma-hang until an interrupt, but it wont _execute_ the interrupt
			this->halted = true;
			return 4;
			break;

			// nop
		case 0x00:
			return 4;
			break;

		case 0x10:
			// STOP -- Halt CPU + LCD until a button is pressed.
			// full instruction is 10 00 but nothing else starts with 10 soooo.
			this->ReadPCByte( ); // get the 00 out of the way.
			//this->stopped = true; // not actually worth anything on an emulator.
			return 4;
			break;

		case 0x3F:
			pFlags->c = !pFlags->c; // Complement carry flag // CCF
			pFlags->n = pFlags->h = 0;
			return 4;
			break;
		case 0x37:
			pFlags->h = pFlags->n = 0; // Set carry flag
			pFlags->c = 1;
			return 4;
			break;
		case 0x2F:// CPL
			this->Registers.af.first = ~this->Registers.af.first; // Complement A (NOT)
			pFlags->n = pFlags->h = 1;
			return 4;
			break;

		case 0x27:
		{
			// TODO: FIX ME
			// Decimal adjust A (DAA)
			// so.... we basically take the binary of A... and make it the actual value. aka if A == 0x4, A then becomes 0100
			// aids. // YO THIS FUNCTION IS EXTREME DOGSHIT I HOPE ITS NEVER ACTUALLY USED ELSE IM DEF RIPPING THIS FROM SOME EMULATOR@!
			// yep. shamelessly ripped.
			auto tmpVal = this->Registers.af.first;
			auto shift_down = [ tmpVal ]( ) { return ( ( tmpVal & 0xF0 ) >> 4 ) & 0xF; };
			auto shifted = shift_down( );
			auto bottom = tmpVal & 0xF;


			if ( !pFlags->n )
			{
				if ( pFlags->c || tmpVal > 0x99 )
				{
					this->Registers.af.first = ( tmpVal + 0x60 ) & 0xFF;
					pFlags->c = true;
				}

				if ( pFlags->h || ( tmpVal & 0xF ) > 9 )
					this->Registers.af.first = ( tmpVal + 6 ) & 0xFF;
			}
			else if ( pFlags->c && pFlags->h )
				this->Registers.af.first = ( tmpVal + 0x9A ) & 0xFF;
			else if ( pFlags->c )
				this->Registers.af.first = ( tmpVal + 0xA0 ) & 0xFF;
			else if ( pFlags->h )
				this->Registers.af.first = ( tmpVal + 0xFA ) & 0xFF;

			pFlags->h = 0;
			pFlags->z = !this->Registers.af.first;
			Log( "DAA called there is no god" ); // 9K jumped to 20K so thats wrong.
			return 4;
			break;
		}
#pragma region POPs
		case 0xC1:
		case 0xD1:
		case 0xE1:
		case 0xF1:
			// POP 16byte
			as_pairs = GetOperandsByIndex( 0, opcode - 0xC1, true );
			Pop( ( uint16_t* )as_pairs.second );
			return 12;
			break;
#pragma endregion

#pragma region PUSHs
		case 0xC5:
		case 0xD5:
		case 0xE5:
		case 0xF5:
			// PUSH 16 byte
			as_pairs = GetOperandsByIndex( 0, opcode - 0xC5, true );
			Push( *( uint16_t* )as_pairs.second );
			return 16;
			break;
#pragma endregion
			//case 0xD5: 
			//	// push de
			//	Push( this->Registers.gp.de.full );
			//	return 1;
			//	break;
			//case 0xE5: 
			//	// push hl
			//	Push( this->Registers.gp.hl.full );
			//	return 1;
			//	break;
			//case 0xF5: 
			//	// push af
			//	Push( this->Registers.af.full );
			//	return 1;
			//	break;

#pragma region Interrupts
		case 0xF3:
			this->toggle_interrupts = SetInterrupt::disable; // disable after next instruction
			return 4;
			break;
		case 0xFB:
			this->toggle_interrupts = SetInterrupt::enable; // enable after next instruction
			return 4;
			break;
#pragma endregion

	}

	return 0;
}

int Z80::HandleInvalid( uint8_t base_opcode, uint8_t opcode )
{
	switch ( opcode )
	{
		case 0xEC:
			this->ReadPCWord( );
			break;
		default:
			break;
	}

	return 4; // treat it like a NOP
}

int Z80::HandleINCDEC( uint8_t base_opcode, uint8_t opcode )
{
	auto as_pairs = GetOperandsByIndex( 0, 0, 0 ); // for auto cause im lazy.
	auto gp = &this->Registers.gp;
	auto pFlags = this->Registers.GetFlags( );
	uint8_t oldVal = 0;
	switch ( opcode )
	{
		default:
			__debugbreak( );
			break;

#pragma region INCS
		case 0x3C:
			oldVal = this->Registers.af.first++;
			pFlags->z = !this->Registers.af.first;
			pFlags->n = 0;
			pFlags->h = test_bit( oldVal, 4 ) != test_bit( this->Registers.af.first, 4 ); // simpler for incs.
																						  // h set if carry from bit 3. ( so bit 4 will be different )
			break;
		case 0x4:
			oldVal = gp->bc.first++;
			pFlags->z = !gp->bc.first;
			pFlags->n = 0;
			pFlags->h = test_bit( oldVal, 4 ) != test_bit( gp->bc.first, 4 );
			// h set if carry from bit 3.
			break;
		case 0xC:
			oldVal = gp->bc.second++;
			pFlags->z = !gp->bc.second;
			pFlags->n = 0;
			pFlags->h = test_bit( oldVal, 4 ) != test_bit( gp->bc.second, 4 );
			// h set if carry from bit 3.
			break;
		case 0x14:
			oldVal = gp->de.first++;
			pFlags->z = !gp->de.first;
			pFlags->n = 0;
			pFlags->h = test_bit( oldVal, 4 ) != test_bit( gp->de.first, 4 );
			break;
		case 0x1C:
			oldVal = gp->de.second++;
			pFlags->z = !gp->de.second;
			pFlags->n = 0;
			pFlags->h = test_bit( oldVal, 4 ) != test_bit( gp->de.second, 4 );
			break;
		case 0x24:
			oldVal = gp->hl.first++;
			pFlags->z = !gp->hl.first;
			pFlags->n = 0;
			pFlags->h = test_bit( oldVal, 4 ) != test_bit( gp->hl.first, 4 );
			break;
		case 0x2c:
			oldVal = gp->hl.second++;
			pFlags->z = !gp->hl.second;
			pFlags->n = 0;
			pFlags->h = test_bit( oldVal, 4 ) != test_bit( gp->hl.second, 4 );
			break;
		case 0x34:
		{
			oldVal = this->Read( gp->hl.full );
			uint8_t new_val = oldVal + 1;
			this->Write( gp->hl.full, new_val );
			pFlags->z = !new_val;
			pFlags->n = 0;
			pFlags->h = test_bit( oldVal, 4 ) != test_bit( new_val, 4 );
			return 12;
		}
		break;
#pragma endregion

#pragma region DECS
		// decs
		case 0x5:
			oldVal = gp->bc.first--;
			pFlags->z = !gp->bc.first;
			pFlags->n = 1;
			pFlags->h = ( oldVal & 0xF ) == 0; // seems logical right
			break;
		case 0x0D:
			oldVal = gp->bc.second--;
			pFlags->z = !gp->bc.second;
			pFlags->n = 1;
			pFlags->h = ( oldVal & 0xF ) == 0; // seems logical right
			break;
		case 0x15:
			oldVal = gp->de.first--;
			pFlags->z = !gp->de.first;
			pFlags->n = 1;
			pFlags->h = ( oldVal & 0xF ) == 0; // seems logical right
			break;
		case 0x1d:
			oldVal = gp->de.second--;
			pFlags->z = !gp->de.second;
			pFlags->n = 1;
			pFlags->h = ( oldVal & 0xF ) == 0; // seems logical right
			break;
		case 0x25:
			oldVal = gp->hl.first--;
			pFlags->z = !gp->hl.first;
			pFlags->n = 1;
			pFlags->h = ( oldVal & 0xF ) == 0; // seems logical right
			break;
		case 0x2D:
			oldVal = gp->hl.second--;
			pFlags->z = !gp->hl.second;
			pFlags->n = 1;
			pFlags->h = ( oldVal & 0xF ) == 0; // seems logical right
			break;
		case 0x35:
			// dec (HL)
			oldVal = this->Read( gp->hl.full );
			this->Write( gp->hl.full, ( uint8_t )( oldVal - 1 ) );
			pFlags->z = !( oldVal - 1 );
			pFlags->n = 1;
			pFlags->h = ( oldVal & 0xF ) == 0; // seems logical right
			return 12;
			break;
		case 0x3D:
			oldVal = this->Registers.af.first--;
			pFlags->z = !this->Registers.af.first;
			pFlags->n = 1;
			pFlags->h = ( oldVal & 0xF ) == 0; // seems logical right
			break;
#pragma endregion

			// dec pairs
		case 0x0b:
		case 0x1b:
		case 0x2b:
			as_pairs = GetOperandsByIndex( 0, opcode - 0x0b, true );
			( *( uint16_t* )as_pairs.second )--;
			return 8;
			break;
		case 0x3B:
			// GetOperands wont pick SP
			this->Registers.sp--;
			return 8;
			break;

			// inc pairs
		case 0x03:
		case 0x13:
		case 0x23:
		case 0x33:
			if ( opcode == 0x33 )
				this->Registers.sp++;
			else
			{
				as_pairs = GetOperandsByIndex( 0, opcode - 0x03, true );
				( *( uint16_t* )as_pairs.second )++;
			}
			return 8;
			break;

	}

	return 4;
}

int Z80::HandleLDPP( uint8_t base_opcode, uint8_t opcode )
{
	auto dst = GetDestRegisterIndex( opcode );
	uint16_t val = 0;
	auto pFlags = this->Registers.GetFlags( );
	//auto val = pc->value;
	switch ( opcode )
	{
		default:
			__debugbreak( );
			break;

		case 0xE8:
		{
			// FAILED.

			// add N to sp
			pFlags->n = 0;
			pFlags->z = 0;
			// idk how theyw ant h and c set with this im too lazy tbh
			auto oldVal = this->Registers.sp;
			auto aids = this->ReadSignedPCByte( );
			// i think... im supposed to add aids to pc??
			auto full_res = ( int )( aids + this->Registers.sp );

			this->Registers.sp = ( uint16_t )full_res;

			pFlags->h = ( ( int )( oldVal & 0xF ) + ( aids & 0xF ) > 0xF ); // carried from bit 4
			pFlags->c = ( ( ( int )( oldVal & 0xFF ) + ( aids & 0xFF ) ) > 0xFF ); // carried from bit 7 ( overflowed )
			return 16;
			break;
		}
		case 0x08:
			// LD (nn), SP
			val = this->ReadPCWord( );
			this->Write( val, this->Registers.sp );
			//*( uint16_t* )this->GetRealAddress( val ) = this->Registers.sp;
			return 20;//!
			break;

		case 0x31:
			val = this->ReadPCWord( );
			this->Registers.sp = val;
			return 12;
		case 0x2A:
			// ldi a, (hl)
			this->Registers.af.first = this->Read( this->Registers.gp.hl.full++ );
			return 8;
			break;
		case 0xF8:
		{
			// FAILED.
			// LDHL SP,n ( LDHL SP+n)
			auto aids = this->ReadSignedPCByte( );
			auto negative = test_bit( aids, 7 );
			auto oldVal = this->Registers.sp;
			auto full_val = oldVal + aids;
			this->Registers.gp.hl.full = ( uint16_t )full_val;
			pFlags->n = pFlags->z = 0;
			// h+c meant to be set or reset according to operation. Presumably just like add/sub
			pFlags->h = ( ( int )( oldVal & 0xF ) + ( int )( aids & 0xF ) > 0xF ); // carried from bit 4
			pFlags->c = ( ( ( int )( oldVal & 0xFF ) + ( aids & 0xFF ) ) > 0xFF ); // carried from bit 7 ( overflowed )
			break;
		}
		case 0xF9:
			this->Registers.sp = this->Registers.gp.hl.full;
			return 8;
			break;

		case 0x01:
		case 0x11:
		case 0x21:
			val = this->ReadPCWord( );
			auto pair = GetOperandsByIndex( dst, 0, true ); // returns as uint16_t*
			*( uint16_t* )pair.first = val;
			break;
	}

	return 12; // if its a pair, then yea, 12.
			   // EXT ones are 4 bytes(!!!!)
}

int Z80::HandleBranchChange( uint8_t base_opcode, uint8_t opcode )
{
	auto pFlags = this->Registers.GetFlags( );
	int16_t word_val = 0;
	int8_t byte_val = 0; // as you cant declare these inside a switch

						 // sub 2 from relative results (instruction size)

	switch ( opcode )
	{
		default:
			__debugbreak( );
			break;

#pragma region Restarts
			// push addr on stack and jump to location
		case 0xC7:
			Push( this->Registers.pc );
			this->Registers.pc = 0x00;
			return 16;
			break;
		case 0xCF:
			Push( this->Registers.pc );
			this->Registers.pc = 0x08;
			return 16;
			break;

		case 0xD7:
			Push( this->Registers.pc );
			this->Registers.pc = 0x10;
			return 16;
			break;

		case 0xDF:
			Push( this->Registers.pc );
			this->Registers.pc = 0x18;
			return 16;
			break;

		case 0xE7:
			Push( this->Registers.pc );
			this->Registers.pc = 0x20;
			return 16;
			break;
		case 0xEF:
			Push( this->Registers.pc );
			this->Registers.pc = 0x28;
			return 16;
			break;
		case 0xF7:
			Push( this->Registers.pc );
			this->Registers.pc = 0x30;
			return 16;
			break;
		case 0xFF:
			//__debugbreak( );
			Push( this->Registers.pc );
			this->Registers.pc = 0x38;
			return 16;
			break;
#pragma endregion

#pragma region Calls

			// calls
		case 0xCD:
			// uncond
			Push( this->Registers.pc + 2 );
			this->Registers.pc = this->ReadPCWord( );
			return 24;
			break;
		case 0xC4:
			//NZ
			word_val = this->ReadPCWord( );
			if ( pFlags->z == 0 )
			{
				Push( this->Registers.pc );
				this->Registers.pc = word_val;
				return 24;
			}
			return 12;
			break;
		case 0xCC:
			//Z
			word_val = this->ReadPCWord( );
			if ( pFlags->z )
			{
				Push( this->Registers.pc );
				this->Registers.pc = word_val;
				return 24;
			}
			return 12;
			break;
		case 0xD4:
			//NC
			word_val = this->ReadPCWord( );
			if ( pFlags->c == 0 )
			{
				Push( this->Registers.pc );
				this->Registers.pc = word_val;
				return 24;
			}
			return 12;
			break;
		case 0xDC:
			//C
			word_val = this->ReadPCWord( );

			if ( pFlags->c )
			{
				Push( this->Registers.pc );
				this->Registers.pc = word_val;
				return 24;
			}
			return 12;
			break;
#pragma endregion

#pragma region JMPS
			// jmps
		case 0x18: // JR
			byte_val = this->ReadSignedPCByte( );
			this->Registers.pc += byte_val;
			return 12;
			break;
		case 0x20: // JR NZ
			byte_val = this->ReadSignedPCByte( );
			if ( !pFlags->z )
			{
				this->Registers.pc += byte_val;
				return 12;
			}
			break;
		case 0x28: // JR Z
			byte_val = this->ReadSignedPCByte( );

			if ( pFlags->z )
			{
				this->Registers.pc += byte_val;
				return 12;
			}
			break;
		case 0x30: // JR NC
			byte_val = this->ReadSignedPCByte( );

			if ( !pFlags->c )
			{
				this->Registers.pc += byte_val;
				return 12;
			}
			break;
		case 0x38: // JR C
			byte_val = this->ReadSignedPCByte( );

			if ( pFlags->c )
			{
				this->Registers.pc += byte_val;
				return 12;
			}
			break;
		case 0xC3: // JP ( uncodintional jump )
			word_val = this->ReadSignedPCWord( );
			this->Registers.pc = word_val;
			return 16;
			break;
		case 0xC2:
			// JP NZ
			word_val = this->ReadSignedPCWord( );
			if ( !pFlags->z )
			{
				this->Registers.pc = word_val;
				return 16;
			}
			return 12;
			break;
		case 0xCA:
			// JP Z
			word_val = this->ReadSignedPCWord( );
			if ( pFlags->z )
			{
				this->Registers.pc = word_val;
				return 16;
			}
			return 12;
			break;
		case 0xD2:
			// JP NC
			word_val = this->ReadSignedPCWord( );
			if ( !pFlags->c )
			{
				this->Registers.pc = word_val;
				return 16;
			}
			return 12;
			break;
		case 0xDA:
			// JP C
			word_val = this->ReadSignedPCWord( );
			if ( pFlags->c )
			{
				this->Registers.pc = word_val;
				return 16;
			}
			return 12;
			break;
		case 0xE9:
			// JP (HL), but is it a dereference or just whatever HL currently is??!?!?!??!?!?!?!?!?!?!?!?!?!?
			//word_val = *( uint16_t* )this->GetRealAddress( this->Registers.gp.hl.full ); // yeah fuck you nintendo
			word_val = this->Registers.gp.hl.full;
			this->Registers.pc = word_val;
			return 4;
			break;
#pragma endregion

#pragma region RETs

		case 0xC0:
			// ret NZ
			if ( !pFlags->z )
			{
				Pop( &this->Registers.pc );
				return 20;
			}
			break;
		case 0xC8:
			// RET Z
			if ( pFlags->z )
			{
				Pop( &this->Registers.pc );
				return 20;
			}
			break;
		case 0xC9:
			// RET
			Pop( &this->Registers.pc );
			return 16;
			break;
		case 0xD0:
			if ( pFlags->c == 0 )
			{
				Pop( &this->Registers.pc );
				return 20;
			}
			break;
		case 0xD8:
			// RET C
			if ( pFlags->c )
			{
				Pop( &this->Registers.pc );
				return 20;
			}
			break;
		case 0xD9:
			// RETI
			Pop( &this->Registers.pc );
			this->toggle_interrupts = SetInterrupt::enable;
			return 16;
			break;
#pragma endregion

	}
	return 8;
}

int Z80::HandleADD( uint8_t base_opcode, uint8_t opcode )
{
	/*
	accumulator += value
	S bit set if negative result
	Z set if 0
	H set if carry from bit 3
	*/
	auto pFlags = this->Registers.GetFlags( );
	switch ( opcode )
	{
		case 0x88:
		case 0x89:
		case 0x8A:
		case 0x8B:
		case 0x8C:
		case 0x8D:
		case 0x8E:
		case 0x8F:
		{
			// ADC
			int aids = 0;
			if ( opcode == 0x8E )
				aids = this->Read( this->Registers.gp.hl.full );
			else
				aids = *GetOperandsByIndex( 0, opcode - 0x88 ).second;

			auto oldVal = this->Registers.af.first;
			auto toAdd = aids + pFlags->c;
			auto full_res = toAdd + this->Registers.af.first;

			this->Registers.af.first = ( uint8_t )full_res;
			pFlags->z = !this->Registers.af.first;
			pFlags->n = 0;

			pFlags->h = ( ( ( oldVal & 0xF ) + ( aids & 0xF ) + pFlags->c ) > 0xF ); // seems logical right
																					 //pFlags->h = test_bit( oldVal, 4 ) != test_bit( this->Registers.af.first, 4 ); // carried from bit  3
			pFlags->c = ( full_res > 0xFF ); // carried from bit  7 (overflowed)
			return 4;
			break;
		}
		case 0xCE:
		{	// ADC(n)
			auto aids = this->ReadPCByte( );
			auto oldVal = this->Registers.af.first;
			auto toAdd = aids + pFlags->c;
			auto full_res = toAdd + this->Registers.af.first;

			this->Registers.af.first = ( uint8_t )full_res;
			pFlags->z = !this->Registers.af.first;
			pFlags->n = 0;
			pFlags->h = ( ( ( oldVal & 0xF ) + ( aids & 0xF ) + pFlags->c ) > 0xF ); // seems logical right
			pFlags->c = ( full_res > 0xFF ); // carried from bit 7 ( overflowed )
			return 8;
			break;
		}
		case 0x09:
		case 0x19:
		case 0x29:
		case 0x39:
		{
			auto aids = *( uint16_t* )GetOperandsByIndex( 0, opcode - 0x09, true ).second;
			if ( opcode == 0x39 )
				aids = this->Registers.sp;
			auto oldVal = this->Registers.gp.hl.full;
			auto full_res = aids + this->Registers.gp.hl.full;
			// add HL, aids
			this->Registers.gp.hl.full = ( uint16_t )full_res;
			pFlags->n = 0;
			pFlags->h = ( ( int )( oldVal & 0xFFF ) + ( int )( aids & 0xFFF ) > 0xFFF ); // carried from bit 11
			pFlags->c = ( full_res > 0xFFFF ); // carried from bit 15 (overflowed)
			return 8;
			break;
		}

		default:
			break;
	}

	auto src = opcode % 8;
	auto oldVal = this->Registers.af.first;
	uint8_t regVal = 0;
	if ( opcode == 0x86 ) // (HL)
		regVal = this->Read( this->Registers.gp.hl.full );
	else if ( opcode == 0xC6 )
		regVal = this->ReadPCByte( );
	else
	{
		auto regs = GetOperandsByIndex( 0, src, false );
		regVal = *regs.second;
	}


	auto full_res = regVal + this->Registers.af.first;
	this->Registers.af.first = ( uint8_t )full_res;
	pFlags->z = !this->Registers.af.first;
	pFlags->n = 0;
	pFlags->h = ( ( oldVal & 0xF ) + ( regVal & 0xF ) > 0xF ); // seems logical right
															   //pFlags->h = test_bit( oldVal, 4 ) != test_bit( this->Registers.af.first, 4 );
	pFlags->c = ( full_res > 0xFF );
	// sub (nn) is 8 cycles.

	return ( opcode == 0x86 || opcode == 0xc6 ) ? 8 : 4;
}

int Z80::HandleSUB( uint8_t base_opcode, uint8_t opcode )
{
	auto src = opcode % 8;
	auto oldVal = this->Registers.af.first;

	uint8_t regVal = 0;
	if ( opcode == 0x96 || opcode == 0x9E ) // (HL)
		regVal = this->Read( this->Registers.gp.hl.full );
	else if ( opcode == 0xD6 || opcode == 0xDE )
		regVal = this->ReadPCByte( );
	else
	{
		auto regs = GetOperandsByIndex( 0, src, false );
		regVal = *regs.second;
	}
	auto pFlags = this->Registers.GetFlags( );

	bool sbc = false;

	switch ( opcode )
	{
		case 0x98:
		case 0x99:
		case 0x9A:
		case 0x9B:
		case 0x9C:
		case 0x9D:
		case 0x9E:
		case 0x9F:
		case 0xDE:
			// SBC
			sbc = true;
			break;
		default:
			break;
	}

	auto full_val = this->Registers.af.first - regVal;
	if ( sbc )
		full_val -= pFlags->c;

	this->Registers.af.first = ( uint8_t )full_val;

	pFlags->z = !this->Registers.af.first;
	pFlags->n = 1;
	if ( sbc )
		pFlags->h = ( ( int )( oldVal & 0xF ) - ( regVal & 0xF ) - pFlags->c ) < 0;
	else
		pFlags->h = ( ( oldVal & 0xF ) - ( regVal & 0xF ) ) < 0;
	pFlags->c = ( full_val < 0 );

	return ( opcode == 0x96 || opcode == 0xD6 ) ? 8 : 4;
}

int Z80::HandleXOR( uint8_t base_opcode, uint8_t opcode )
{
	auto pFlags = this->Registers.GetFlags( );
	switch ( opcode )
	{
		default:
			__debugbreak( );
			break;
		case 0xA8:
		case 0xA9:
		case 0xAA:
		case 0xAB:
		case 0xAC:
		case 0xAD:
		case 0xAE:
		case 0xAF:
		case 0xEE:
		{
			// XOR
			pFlags->n = pFlags->h = pFlags->c = 0;
			auto src_reg = GetOperandsByIndex( 0, opcode - 0xA8 ).second;
			if ( opcode == 0xEE )
				this->Registers.af.first ^= this->ReadPCByte( );
			else if ( opcode == 0xAE )
				this->Registers.af.first ^= this->Read( this->Registers.gp.hl.full );
			else
				this->Registers.af.first ^= *src_reg;

			pFlags->z = !this->Registers.af.first;
			if ( opcode == 0xAE || opcode == 0xEE )
				return 8;
			else
				return 4;
			break;
		}
	}
}

int Z80::HandleAND( uint8_t base_opcode, uint8_t opcode )
{
	switch ( opcode )
	{
		default:
			__debugbreak( );
			break;

		case 0xA0:
		case 0xA1:
		case 0xA2:
		case 0xA3:
		case 0xA4:
		case 0xA5:
		case 0xA6:
		case 0xA7:
		case 0xE6:
		{// and x with a
			auto pFlags = this->Registers.GetFlags( );

			if ( opcode == 0xE6 )
			{
				auto tmpVal = this->ReadPCByte( );
				this->Registers.af.first &= tmpVal;
				pFlags->z = !this->Registers.af.first;
				pFlags->n = pFlags->c = 0;
				pFlags->h = 1;
				return 8;
			}
			else
			{
				auto paired = GetOperandsByIndex( 0, opcode - 0xA0 );
				if ( opcode == 0xA6 )
					this->Registers.af.first &= this->Read( this->Registers.gp.hl.full );
				else
					this->Registers.af.first &= *paired.second;
				pFlags->z = !this->Registers.af.first;
				pFlags->n = pFlags->c = 0;
				pFlags->h = 1;
			}
			if ( opcode == 0xA6 )
				return 8;
			else
				return 4;
			break;
		}
	}
}

int Z80::HandleCMP( uint8_t base_opcode, uint8_t opcode )
{
	auto pFlags = this->Registers.GetFlags( );

	switch ( opcode )
	{
		default:
			__debugbreak( );
			break;

		case 0xB8:
		case 0xB9:
		case 0xBA:
		case 0xBB:
		case 0xBC:
		case 0xBD:
		case 0xBE:
		case 0xBF:
		case 0xFE:
		{
			// CMP <REG>
			// compare A with xyz
			auto tmpVal = this->Registers.af.first;
			uint8_t cmpVal = 0;
			if ( opcode == 0xFE )
				cmpVal = this->ReadPCByte( );
			else if ( opcode == 0xBE )
				cmpVal = this->Read( this->Registers.gp.hl.full );
			else
			{
				auto src = GetSrcRegisterIndex( opcode );
				auto paired = GetOperandsByIndex( 0, src );
				auto src_reg = paired.second;
				cmpVal = *src_reg; // cant be bothered to change rthis function sooooo work around
			}
			auto full_val = tmpVal - cmpVal;

			pFlags->z = ( full_val == 0 );
			pFlags->n = 1;
			pFlags->h = ( ( tmpVal & 0xF ) - ( cmpVal & 0xF ) ) < 0; // as CMPS are essentially val1-val2 with the result thrown away
																	 //pFlags->h = test_bit( tmpVal, 4 ) == test_bit( tmpVal - cmpVal, 4 ); // as CMPS are essentially val1-val2 with the result thrown away
			pFlags->c = ( full_val < 0 );
			// h set if no borrow from bit 4.
			if ( opcode == 0xBE || opcode == 0xFE )
				return 8;
			else
				return 4;
			break;
		}
	}
}

int Z80::HandleOR( uint8_t base_opcode, uint8_t opcode )
{
	auto pFlags = this->Registers.GetFlags( );
	switch ( opcode )
	{
		default:
			__debugbreak( );
			break;

		case 0xB0:
		case 0xB1:
		case 0xB2:
		case 0xB3:
		case 0xB4:
		case 0xB5:
		case 0xB6:
		case 0xB7:
		case 0xF6:
			// OR this value with A.
			pFlags->n = pFlags->h = pFlags->c = 0;
			if ( opcode == 0xF6 )
				this->Registers.af.first |= this->ReadPCByte( );
			else
			{
				if ( opcode == 0xB6 )
				{
					this->Registers.af.first |= this->Read( this->Registers.gp.hl.full );
				}
				else
				{
					auto src = GetSrcRegisterIndex( opcode );
					auto paired = GetOperandsByIndex( 0, src );
					auto src_reg = paired.second;
					this->Registers.af.first |= *src_reg;
				}
			}
			pFlags->z = !this->Registers.af.first;
			return ( opcode == 0xF6 || opcode == 0xb6 ) ? 8 : 4;
			break;
	}
}

int Z80::HandleBitwise( uint8_t base_opcode, uint8_t opcode )
{
	auto pFlags = this->Registers.GetFlags( );
	auto src = GetSrcRegisterIndex( opcode );
	auto paired = GetOperandsByIndex( 0, src );
	auto src_reg = paired.second;
	uint8_t cmpVal = 0;
	uint8_t tmpVal = 0;

	switch ( opcode )
	{
		default:
			__debugbreak( );
			break;

#pragma region Rotates
		case 0x07:
		case 0x17:
			// RLCA -- 0x07 (Rotate A left carry A)
			// RLA  -- 0x17 (Rotate A left through carry)
			pFlags->n = pFlags->h = pFlags->z = 0;
			tmpVal = this->Registers.af.first;
			this->Registers.af.first <<= 1;
			if ( opcode == 0x17 ) // RLA 
				this->Registers.af.first |= pFlags->c;
			else	// RLCA
				this->Registers.af.first |= get_bit( tmpVal, 7 );

			pFlags->c = get_bit( tmpVal, 7 );
			return 4;
			break;


		case 0xF:// RRCA
		case 0x1F:// RRA (through carry)
			pFlags->n = pFlags->h = pFlags->z = 0;
			tmpVal = this->Registers.af.first;

			this->Registers.af.first >>= 1;
			if ( opcode == 0x1F )
				this->Registers.af.first |= ( pFlags->c << 7 );
			else
				this->Registers.af.first |= ( get_bit( tmpVal, 0 ) << 7 );

			pFlags->c = get_bit( tmpVal, 0 );
			return 4;
			break;
#pragma endregion

#pragma region CB Prefixed
		case 0xCB:
			// prefixed.
			switch ( cmpVal = this->ReadPCByte( ), cmpVal )
			{
				default:
					__debugbreak( );
					break;

					// TODO: make this sleeker.
#pragma region BITS
				case 0x40:
				case 0x41:
				case 0x42:
				case 0x43:
				case 0x44:
				case 0x45:
				case 0x46:
				case 0x47:
				case 0x48:
				case 0x49:
				case 0x4A:
				case 0x4B:
				case 0x4C:
				case 0x4D:
				case 0x4E:
				case 0x4F:
				case 0x50: // still bits...
				case 0x51:
				case 0x52:
				case 0x53:
				case 0x54:
				case 0x55:
				case 0x56:
				case 0x57:
				case 0x58:
				case 0x59:
				case 0x5A:
				case 0x5B:
				case 0x5C:
				case 0x5D:
				case 0x5E:
				case 0x5F:
				case 0x60:
				case 0x61:
				case 0x62:
				case 0x63:
				case 0x64:
				case 0x65:
				case 0x66:
				case 0x67:
				case 0x68:
				case 0x69:
				case 0x6A:
				case 0x6B:
				case 0x6C:
				case 0x6D:
				case 0x6E:
				case 0x6F:
				case 0x70:
				case 0x71:
				case 0x72:
				case 0x73:
				case 0x74:
				case 0x75:
				case 0x76:
				case 0x77:
				case 0x78:
				case 0x79:
				case 0x7A:
				case 0x7B:
				case 0x7C:
				case 0x7D:
				case 0x7E:
				case 0x7F:
				{
					auto val = *GetOperandsByIndex( 0, ( cmpVal - 0x40 ) % 8 ).second;
					if ( ( uint8_t )SRCOperand::HL == ( ( cmpVal - 0x40 ) % 8 ) )
						val = this->Read( this->Registers.gp.hl.full );

					auto bit = ( cmpVal - 0x40 ) / 8;
					pFlags->n = 0, pFlags->h = 1;
					pFlags->z = !( test_bit( val, bit ) );

					if ( ( cmpVal % 8 ) == 0x6 )
						return 16;
					else
						return 8;
					break;
				}
#pragma endregion

#pragma region RES
				case 0x80:
				case 0x81:
				case 0x82:
				case 0x83:
				case 0x84:
				case 0x85:
				case 0x86:
				case 0x87:
				case 0x88:
				case 0x89:
				case 0x8A:
				case 0x8B:
				case 0x8C:
				case 0x8D:
				case 0x8E:
				case 0x8F:
				case 0x90: // still bits...
				case 0x91:
				case 0x92:
				case 0x93:
				case 0x94:
				case 0x95:
				case 0x96:
				case 0x97:
				case 0x98:
				case 0x99:
				case 0x9A:
				case 0x9B:
				case 0x9C:
				case 0x9D:
				case 0x9E:
				case 0x9F:
				case 0xA0:
				case 0xA1:
				case 0xA2:
				case 0xA3:
				case 0xA4:
				case 0xA5:
				case 0xA6:
				case 0xA7:
				case 0xA8:
				case 0xA9:
				case 0xAA:
				case 0xAB:
				case 0xAC:
				case 0xAD:
				case 0xAE:
				case 0xAF:
				case 0xB0:
				case 0xB1:
				case 0xB2:
				case 0xB3:
				case 0xB4:
				case 0xB5:
				case 0xB6:
				case 0xB7:
				case 0xB8:
				case 0xB9:
				case 0xBA:
				case 0xBB:
				case 0xBC:
				case 0xBD:
				case 0xBE:
				case 0xBF:
				{
					//res
					auto val = GetOperandsByIndex( 0, ( cmpVal - 0x80 ) % 8 ).second;
					auto bit = ( cmpVal - 0x80 ) / 8;
					if ( ( uint8_t )SRCOperand::HL == ( ( cmpVal - 0x80 ) % 8 ) )
					{
						auto bit_val = this->Read( this->Registers.gp.hl.full );
						set_bit( bit_val, bit, 0 );
						this->Write( this->Registers.gp.hl.full, bit_val );
					}
					else
						set_bit( *val, bit, 0 );


					if ( ( cmpVal % 8 ) == 0x6 )
						return 16;
					else
						return 8;
					break;
				}
#pragma endregion

#pragma region SET
				case 0xC0:
				case 0xC1:
				case 0xC2:
				case 0xC3:
				case 0xC4:
				case 0xC5:
				case 0xC6:
				case 0xC7:
				case 0xC8:
				case 0xC9:
				case 0xCA:
				case 0xCB:
				case 0xCC:
				case 0xCD:
				case 0xCE:
				case 0xCF:
				case 0xD0: // still bits...
				case 0xD1:
				case 0xD2:
				case 0xD3:
				case 0xD4:
				case 0xD5:
				case 0xD6:
				case 0xD7:
				case 0xD8:
				case 0xD9:
				case 0xDA:
				case 0xDB:
				case 0xDC:
				case 0xDD:
				case 0xDE:
				case 0xDF:
				case 0xE0:
				case 0xE1:
				case 0xE2:
				case 0xE3:
				case 0xE4:
				case 0xE5:
				case 0xE6:
				case 0xE7:
				case 0xE8:
				case 0xE9:
				case 0xEA:
				case 0xEB:
				case 0xEC:
				case 0xED:
				case 0xEE:
				case 0xEF:
				case 0xF0:
				case 0xF1:
				case 0xF2:
				case 0xF3:
				case 0xF4:
				case 0xF5:
				case 0xF6:
				case 0xF7:
				case 0xF8:
				case 0xF9:
				case 0xFA:
				case 0xFB:
				case 0xFC:
				case 0xFD:
				case 0xFE:
				case 0xFF:
				{
					auto val = GetOperandsByIndex( 0, ( cmpVal - 0xC0 ) % 8 ).second;
					auto bit = ( cmpVal - 0xC0 ) / 8;
					if ( ( uint8_t )SRCOperand::HL == ( ( cmpVal - 0xC0 ) % 8 ) )
					{
						auto bit_val = this->Read( this->Registers.gp.hl.full );
						set_bit( bit_val, bit, 1 );
						this->Write( this->Registers.gp.hl.full, bit_val );
					}
					else
						set_bit( *val, bit, 1 );

					if ( ( cmpVal % 8 ) == 0x6 )
						return 16;
					else
						return 8;
					break;
				}
#pragma endregion

#pragma region Rotates
				case 0x00:
				case 0x01:
				case 0x02:
				case 0x03:
				case 0x04:
				case 0x05:
				case 0x06:
				case 0x07:
				{
					// RLC (n)
					pFlags->n = pFlags->h = 0;
					auto reg = GetOperandsByIndex( 0, cmpVal ).second;
					if ( cmpVal == 0x6 )
					{
						auto notPointer = this->Read( this->Registers.gp.hl.full );
						tmpVal = notPointer;
						notPointer <<= 1;
						set_bit( notPointer, 0, get_bit( tmpVal, 7 ) );
						this->Write( this->Registers.gp.hl.full, notPointer );

						pFlags->c = get_bit( tmpVal, 7 );
						pFlags->z = !notPointer;
					}
					else
					{
						tmpVal = *reg;
						*reg <<= 1;
						set_bit( *reg, 0, get_bit( tmpVal, 7 ) );

						pFlags->c = get_bit( tmpVal, 7 );
						pFlags->z = !*reg;
					}
					if ( cmpVal == 0x6 )
						return 16;
					else
						return 8;
					break;
				}

				case 0x08:
				case 0x09:
				case 0x0A:
				case 0x0B:
				case 0x0C:
				case 0x0D:
				case 0x0E:
				case 0x0F:
					// RRC (n)
					paired = GetOperandsByIndex( 0, cmpVal - 0x8 );
					pFlags->n = pFlags->h = 0;
					if ( cmpVal == 0xE )
					{
						auto notPointer = this->Read( this->Registers.gp.hl.full );
						tmpVal = notPointer;
						notPointer >>= 1;
						set_bit( notPointer, 7, get_bit( tmpVal, 0 ) );
						this->Write( this->Registers.gp.hl.full, notPointer );

						pFlags->c = get_bit( tmpVal, 0 );
						pFlags->z = !notPointer;
					}
					else
					{
						tmpVal = *paired.second;

						*paired.second >>= 1;
						set_bit( *paired.second, 7, get_bit( tmpVal, 0 ) );

						pFlags->c = get_bit( tmpVal, 0 );
						pFlags->z = !*paired.second;
					}
					if ( cmpVal == 0xE )
						return 16;
					else
						return 8;
					break;

				case 0x10:
				case 0x11:
				case 0x12:
				case 0x13:
				case 0x14:
				case 0x15:
				case 0x16:
				case 0x17:
					//RL
					paired = GetOperandsByIndex( 0, cmpVal - 0x10 );
					pFlags->n = pFlags->h = 0;
					if ( cmpVal == 0x16 )
					{
						auto notPointer = this->Read( this->Registers.gp.hl.full );
						tmpVal = notPointer;
						notPointer <<= 1;
						set_bit( notPointer, 0, pFlags->c );
						this->Write( this->Registers.gp.hl.full, notPointer );

						pFlags->c = get_bit( tmpVal, 7 );
						pFlags->z = !notPointer;
					}
					else
					{
						tmpVal = *paired.second;

						*paired.second <<= 1;
						set_bit( *paired.second, 0, pFlags->c );
						pFlags->c = get_bit( tmpVal, 7 );
						pFlags->z = !*paired.second;
					}
					if ( cmpVal == 0x16 )
						return 16;
					else
						return 8;
					break;

				case 0x18:
				case 0x19:
				case 0x1A:
				case 0x1B:
				case 0x1C:
				case 0x1D:
				case 0x1E:
				case 0x1F:
					// RR
					paired = GetOperandsByIndex( 0, cmpVal - 0x18 );
					pFlags->n = pFlags->h = 0;
					if ( cmpVal == 0x1E )
					{
						auto notPointer = this->Read( this->Registers.gp.hl.full );
						tmpVal = notPointer;
						notPointer >>= 1;
						set_bit( notPointer, 7, pFlags->c );
						this->Write( this->Registers.gp.hl.full, notPointer );

						pFlags->c = get_bit( tmpVal, 0 );
						pFlags->z = !notPointer;
					}
					else
					{
						tmpVal = *paired.second;

						*paired.second >>= 1;
						set_bit( *paired.second, 7, pFlags->c );
						pFlags->c = get_bit( tmpVal, 0 );
						pFlags->z = !*paired.second;
					}
					if ( cmpVal == 0x1E )
						return 16;
					else
						return 8;
					break;

				case 0x20:
				case 0x21:
				case 0x22:
				case 0x23:
				case 0x24:
				case 0x25:
				case 0x26:
				case 0x27:
					// SLA
					paired = GetOperandsByIndex( 0, cmpVal - 0x20 );
					pFlags->n = pFlags->h = 0;
					if ( cmpVal == 0x26 )
					{
						auto notPointer = this->Read( this->Registers.gp.hl.full );
						tmpVal = notPointer;
						notPointer <<= 1;
						this->Write( this->Registers.gp.hl.full, notPointer );

						pFlags->c = get_bit( tmpVal, 7 );
						pFlags->z = !notPointer;
					}
					else
					{
						tmpVal = *paired.second;

						*paired.second <<= 1;
						pFlags->c = get_bit( tmpVal, 7 );
						pFlags->z = !*paired.second;
					}
					if ( cmpVal == 0x26 )
						return 16;
					else
						return 8;
					break;

				case 0x28:
				case 0x29:
				case 0x2A:
				case 0x2B:
				case 0x2C:
				case 0x2D:
				case 0x2E:
				case 0x2F:
					// SRA
					paired = GetOperandsByIndex( 0, cmpVal - 0x28 );
					pFlags->n = pFlags->h = 0;
					if ( cmpVal == 0x2E )
					{
						auto notPointer = this->Read( this->Registers.gp.hl.full );
						tmpVal = notPointer;
						notPointer >>= 1;
						set_bit( notPointer, 7, get_bit( tmpVal, 7 ) );
						this->Write( this->Registers.gp.hl.full, notPointer );

						pFlags->c = get_bit( tmpVal, 0 );
						pFlags->z = !notPointer;
					}
					else
					{
						tmpVal = *paired.second;

						*paired.second >>= 1;
						// MSB doesnt change in SRA.
						set_bit( *paired.second, 7, get_bit( tmpVal, 7 ) );
						pFlags->c = get_bit( tmpVal, 0 );
						pFlags->z = !*paired.second;
					}
					if ( cmpVal == 0x2E )
						return 16;
					else
						return 8;
					break;

				case 0x38:
				case 0x39:
				case 0x3A:
				case 0x3B:
				case 0x3C:
				case 0x3D:
				case 0x3E:
				case 0x3F:
					// SRL
					paired = GetOperandsByIndex( 0, cmpVal - 0x38 );
					pFlags->n = pFlags->h = 0;
					if ( cmpVal == 0x3E )
					{
						auto notPointer = this->Read( this->Registers.gp.hl.full );
						tmpVal = notPointer;
						notPointer >>= 1;
						this->Write( this->Registers.gp.hl.full, notPointer );

						pFlags->c = get_bit( tmpVal, 0 );
						pFlags->z = !notPointer;
					}
					else
					{
						tmpVal = *paired.second;

						// set MSB to 0 for SR(x)
						*paired.second >>= 1;
						//set_bit( *paired.second, 7, 0 );
						pFlags->c = get_bit( tmpVal, 0 );
						pFlags->z = !*paired.second;
					}
					if ( cmpVal == 0x3E )
						return 16;
					else
						return 8;
					break;
#pragma endregion

#pragma region SWAP
				case 0x30:
				case 0x31:
				case 0x32:
				case 0x33:
				case 0x34:
				case 0x35:
				case 0x36:
				case 0x37:
					// SWAP
					// Swap upper and lower nibbles
					paired = GetOperandsByIndex( 0, cmpVal - 0x30 );
					if ( cmpVal == 0x36 )
					{
						auto notPointer = this->Read( this->Registers.gp.hl.full );
						tmpVal = notPointer;
						notPointer = ( ( tmpVal & 0xF ) << 4 | ( tmpVal & 0xF0 ) >> 4 );
						this->Write( this->Registers.gp.hl.full, notPointer );
						pFlags->n = pFlags->h = pFlags->c = 0;
						pFlags->z = !notPointer;
					}
					else
					{
						tmpVal = *paired.second;
						*paired.second = ( ( tmpVal & 0xF ) << 4 | ( tmpVal & 0xF0 ) >> 4 );
						pFlags->z = !*paired.second;
						pFlags->n = pFlags->h = pFlags->c = 0;
					}
					return 8;
					break;
#pragma endregion
			}
			break;
#pragma endregion
	}

	return 1;
}

int Z80::HandleLDImm( uint8_t base_opcode, uint8_t opcode )
{
	auto dst = GetDestRegisterIndex( opcode );
	auto paired = GetOperandsByIndex( dst, 0 );
	auto dst_reg = paired.first;

	switch ( opcode )
	{
		case 0x3E:
			this->Registers.af.first = this->ReadPCByte( );
			return 8;
			break;

		case 0x06:
		case 0x0E:
		case 0x16:
		case 0x1E:
		case 0x26:
		case 0x2E:
		case 0x36:
			// 1 byte to register
			if ( opcode == 0x36 )
				this->Write( this->Registers.gp.hl.full, this->ReadPCByte( ) );
			else
				*dst_reg = this->ReadPCByte( );
			return 8; // inc pc by 2
			break;
		default:
			__debugbreak( );
			break;
	}
}

int Z80::HandleLDReg( uint8_t base_opcode, uint8_t opcode )
{
	auto src = opcode % 8;
	auto dst = GetDestRegisterIndex( opcode );
	dst -= base_opcode;

	auto regs = GetOperandsByIndex( dst, src );
	// unsure if we set the top 8 bits of a 16 byte register to 0 if we only move 8 bytes. Prepare for crashes i guess
	if ( dst == ( int )DSTOperand::HL )
		this->Write( this->Registers.gp.hl.full, *regs.second );
	else if ( src == ( int )SRCOperand::HL )
		*regs.first = this->Read( this->Registers.gp.hl.full );
	else
		*regs.first = *regs.second;
	// dest = src

	/*
	Can expand this concept to other things, eventually probably want a nice hub that just hands instructions off to their various handlers
	*/

	if ( opcode == 0x2 || opcode == 0x77 || dst == ( int )DSTOperand::HL )
		return 8;
	else
		return 4;
}

int Z80::HandleLDMem( uint8_t base_opcode, uint8_t opcode )
{
	switch ( opcode )
	{
		default:
			__debugbreak( );
			break;

		case 0xF0:
			// LD A, 0xFF00+n
			this->Registers.af.first = this->Read( 0xFF00 + this->ReadPCByte( ) );
			return 12;
			break;
		case 0xF2:
			// LD A, 0xFF00+C
			this->Registers.af.first = this->Read( 0xFF00 + this->Registers.gp.bc.second );
			return 8;
			break;
		case 0xE2:
			// LD 0xFF00 + registerC, A
			this->Write( 0xFF00 + this->Registers.gp.bc.second, this->Registers.af.first );
			return 8;
			break;

		case 0xE0:
		{
			// LD FF00 + n , A
			auto loc = ( 0xFF00 + this->ReadPCByte( ) );
			this->Write( loc, this->Registers.af.first );
			// if location is 0xFF50, we turn off DMG ROM (aka switch BIOS from being 0x0 to making our rom 0x0 i think)
			if ( loc == 0xFF50 && this->dmg_enabled )
				this->dmg_enabled = false;

			return 12;
			break;
		}
		case 0xEA:
			// LD (nn), a
			this->Write( this->ReadPCWord( ), this->Registers.af.first );
			return 16;
			break;

		case 0x0A:
		case 0x1A:
		{// ld a (bc)/(de)
			auto src_reg = this->GetOperandsByIndex( 0, opcode - 0xA, true );

			this->Registers.af.first = this->Read( *( uint16_t* )src_reg.second );
			return 8;
			break;
		}
		case 0x7E:
			// ld a (hl)
			this->Registers.af.first = this->Read( this->Registers.gp.hl.full );
			return 8;
			break;
		case 0xFA:
			// ld a, (nn)
			this->Registers.af.first = this->Read( this->ReadPCWord( ) );
			return 16;
			break;

		case 0x22:
			// ldi (hl), a
			this->Write( this->Registers.gp.hl.full++, this->Registers.af.first );
			return 8;
			break;
		case 0x32:
			// LDD (HL), A
			// put A into [HL] and decrement HL
			this->Write( this->Registers.gp.hl.full--, this->Registers.af.first );
			return 8;
			break;
		case 0x3A:
			// LDD A, (HL)
			this->Registers.af.first = this->Read( this->Registers.gp.hl.full-- );
			return 8;
			break;

		case 0x02:
		case 0x12:
		{
			// LD (BC/DE), A
			auto src_reg = GetOperandsByIndex( 0, opcode - 0x02, true );
			this->Write( *( uint16_t* )src_reg.second, this->Registers.af.first );
			return 8;
			break;
		}
	}

	return 1;
}

int Z80::UnimplementedInstruction( uint8_t, uint8_t )
{
	Log( "Not yet implemented" );
	__debugbreak( );
	return 0;
}

int Z80::HandleLDSP( uint8_t, uint8_t )
{
	// do we have to care about cycles and stuff? idk. if yes, then we might have to do this during a mem_read cycle or something
	// yes, we do have to care about cycles and stuff.
	auto val = this->ReadPCWord( );
	this->Registers.sp = val;
	__debugbreak( ); // cycles not set.
	return 3;
}