#include "Z80.hpp"
#include "../GBDefines.hpp"



void Z80::Pop( uint16_t* pDest )
{
	*pDest = *( uint16_t* )this->GetRealAddress( this->Registers.sp );
	this->Registers.sp += 2;
}

void Z80::Push( uint16_t value )
{
	this->Registers.sp -= 2;
	*( uint16_t* )this->GetRealAddress( this->Registers.sp ) = value;
}

/*
How flags are set (only on Arithmetic/logical instructions):
S = result is negative
Z/E = result is zero || compares match
H = Half Carry -- Set if a carry happens from bit 3 to bit 4 (what in the FUCK?), or if a borrow from bit 4 occurs (?!?!?!?)
P/V if parity is even or overflow/underflow happened
N = Add/Subtract -- 1 == Sub, 0 == ADD, (Why thef uck?)
Used for DAA instruction apparently

C = If an ADD created a carry || sub created a borrow
*/

uint8_t Z80::ReadPCByte( )
{
	return *this->GetRealAddress( this->Registers.pc++ );
}

uint16_t Z80::ReadPCWord( )
{
	auto ret = *( uint16_t* )this->GetRealAddress( this->Registers.pc );
	this->Registers.pc += 2;
	return ret;
}

int8_t Z80::ReadSignedPCByte( )
{
	return *( int8_t * )this->GetRealAddress( this->Registers.pc++ );
}

int16_t Z80::ReadSignedPCWord( )
{
	auto ret = *( int16_t* )this->GetRealAddress( this->Registers.pc );
	this->Registers.pc += 2;
	return ret;
}

void Z80::SetCartridge( char * cart, CartridgeType type )
{
	this->pCartridge = cart;
	this->cartType = type;

	bool mbc1 = ( cartType == CartridgeType::MBC1 || cartType == CartridgeType::MBC1_RAM || cartType == CartridgeType::MBC1_RAM_BATTERY );
	if ( mbc1 )
		this->mbcType = MBCType::mbc1;
	else
	{
		bool mbc2 = ( cartType == CartridgeType::MBC2 || cartType == CartridgeType::MBC2_BATTERY );
		if ( mbc2 )
			this->mbcType = MBCType::mbc2;
		else
		{
			bool mbc3 = ( cartType == CartridgeType::MBC3_RAM_BATTERY );
			if ( mbc3 )
				this->mbcType = MBCType::mbc3;
		}
	}

}

void Z80::SetRam( char * ram )
{
	this->pRam = ram;
}

bool Z80::Run( unsigned char * data )
{
	static bool bRunning = false;
	if ( this->pRam == nullptr )
	{
		Log( "No ram you dingus" );
		return false;
	}

	if ( !bRunning )
	{
		//this->pData = data;
		bRunning = true;
		this->Registers.pc = 0;

		while ( bRunning ) // ?
			this->Step( );
	}
	else
	{
		Log( "Already running" );
	}

	return false;
}

int Z80::Step( )
{
	if ( !this->pData )
	{
		Log( "Can you fucking initialize you idiot" );
		__debugbreak( );
		return 0;
	}

	// handle interrupt if they exist
	this->HandleInterrupts( );

	if ( this->halted /*|| this->stopped*/ )
		return 4;

	auto cByte = this->ReadPCByte( );

	OpcodeEntry* rPair = nullptr;
	bool found = false;
	for ( auto& x : opcode_array )
	{
		if ( x.has_range == false )
		{
			if ( x.opcode == cByte )
			{
				rPair = &x;
				found = true;
				break;
			}
		}
		else
		{
			if ( cByte >= x.opcode && cByte <= x.range )
			{
				rPair = &x;
				found = true;
				break;
			}
		}
	}

	if ( !found )
	{
		Log( "Unknown opcode %02X at pc %X", cByte, this->Registers.pc - 1 );
		__debugbreak( );
	}
	else
	{
		auto savedPc = ( uint16_t )( this->Registers.pc - 1 );


		auto callback = rPair->Handler;
		//before
		auto cycles = ( this->*callback )( rPair->opcode, cByte );
		// regardless of what happens, lower 4 bits of F is always set to 0 on GB.
		auto pFlags = this->Registers.GetFlags( );
		pFlags->ignored = 0;
		this->elapsed_cycles += cycles;
		if ( cycles == 0 )
			__debugbreak( );

		return cycles;
	}

	return 0;
}

bool Z80::IsStopped( )
{
	return this->stopped;
}

bool Z80::IsHalted( )
{
	return this->halted;
}

bool Z80::IsDMGEnabled( )
{
	return this->dmg_enabled;
}

char * Z80::GetCartridge( )
{
	return this->pCartridge;
}

CPURegisters * Z80::GetRegisters( )
{
	return &this->Registers;
}

void Z80::HandleInterrupts( )
{
	// check if we should be firing on an interrupt
	// priority is: vblank, lcdc, timer, serial, transition
	// only 1 interrupt can be allowed to happen per cycle, once done, the IF register (0xFF0F) should be fed 0 at the interrupt that was fired.

	auto shouldFire = ( bool )( this->pMem->big_union.interrupt_flag.interrupts & this->pMem->big_union.interrupts_enabled );
	if ( shouldFire )
	{
		this->halted = false; // interrupts stop halt
		// check for IME here, because HALTs need to be aware that an interrupt attempted to fire, however if interrupts were disabled, then we skip them.

		if ( this->ime )
		{
			Push( this->Registers.pc );
			auto val = &this->pMem->big_union.interrupt_flag.interrupts;
			if ( test_bit( *val, 0 ) )
			{
				// vblank
				// set pc to interrupt vector, remove vblank flag in interrupt_flag and set iem to 0.
				this->Registers.pc = 0x40;
				( *val ) &= ~( 1 );
				this->ime = false;
				return;
			}
			else if ( test_bit( *val, 1 ) )
			{
				// lcdc
				this->Registers.pc = 0x48;
				( *val ) &= ~( 2 );
				this->ime = false;
				return;
			}
			else if ( test_bit( *val, 2 ) )
			{
				// timer overflow
				this->Registers.pc = 0x50;
				( *val ) &= ~( 1 << 2 );
				this->ime = false;
				return;
			}
			else if ( test_bit( *val, 3 ) )
			{
				// serial transfer done
				this->Registers.pc = 0x58;
				( *val ) &= ~( 1 << 3 );
				this->ime = false;
				return;
			}
			else if ( test_bit( *val, 4 ) )
			{
				// pin transition
				this->Registers.pc = 0x50;
				( *val ) &= ~( 1 << 4 );
				this->ime = false;
				//this->stopped = false
				return;
			}
		}
	}

	if ( this->toggle_interrupts == SetInterrupt::disable )
	{
		this->ime = 0;
		this->toggle_interrupts = SetInterrupt::no_change;
	}
	else if ( this->toggle_interrupts == SetInterrupt::enable )
	{
		this->ime = 1;
		this->toggle_interrupts = SetInterrupt::no_change;
	}
}

int Z80::GetDestRegisterIndex( int full_op )
{
	return full_op - ( full_op % 8 );
}

int Z80::GetSrcRegisterIndex( int full_op )
{
	return ( full_op % 8 );
}

bool Z80::ReadAddressCheck( uint16_t Location )
{
	static bool mbc1 = ( cartType == CartridgeType::MBC1 || cartType == CartridgeType::MBC1_RAM || cartType == CartridgeType::MBC1_RAM_BATTERY );
	static bool mbc2 = ( cartType == CartridgeType::MBC2 || cartType == CartridgeType::MBC2_BATTERY );
	static bool mbc3 = ( cartType == CartridgeType::MBC3_RAM_BATTERY );

	if ( Location >= 0xA000 && Location <= 0xBFFF )
	{
		if ( this->ram_enabled )
			return true;
		else
			return false;
	}


	return true;
}

bool Z80::WriteAddressCheck( uint16_t Location, uint8_t lowval )
{
	// do checks for specific writes because gameboy is cocks.
	//https://gekkio.fi/files/gb-docs/gbctr.pdf
	if ( cartType != CartridgeType::ROM_ONLY )
	{
		bool mbc1 = this->mbcType == MBCType::mbc1;
		bool mbc2 = this->mbcType == MBCType::mbc2;
		bool mbc3 = this->mbcType == MBCType::mbc3;

		if ( Location >= 0 && Location <= 0x1FFF )
		{
			if ( ( mbc2 && !get_bit( Location, 8 ) ) || mbc1 || mbc3 )
				this->ram_enabled = ( ( lowval & 0xF ) == 0b1010 );
		}
		else if ( Location >= 0x2000 && Location <= 0x3FFF )
		{
			if ( mbc2 && !get_bit( Location, 8 ) )
				return false;

			if ( ( lowval & 0xF ) == 0 && ( mbc1 || mbc2 ) )
				lowval |= 1; // apparently not allowed. fucking gameboy.
			else if ( lowval == 0 && mbc3 )
				lowval = 1;
			if ( mbc3 )
				this->bank_register = lowval & 0x7F;
			else
				this->bank_bits.bank_register_1 = lowval;
		}
		else if ( Location >= 0x4000 && Location <= 0x5FFF )
		{
			if ( lowval <= 3 )
				this->ram_bank = lowval;
		}
		else if ( Location >= 0x6000 && Location <= 0x7FFF )
			this->mbc_mode = lowval & 1;
		else if ( Location >= 0xA000 && Location <= 0xBFFF )
		{
			if ( Location <= 0xA1FF )
			{
				if ( mbc2 )
					__debugbreak( );
			}
		}
	}

	if ( Location == 0xFF46 ) // DMA transfer sprites to OAM address.
		memcpy( &this->pRam[ Constants::sprite_attribute_loc ], this->GetRealAddress( lowval * 0x100 ), sizeof( uint32_t ) * 40 );
	else if ( Location == 0xFF44 )
		this->pMem->big_union.ly = 0;

// check if we're writing to a read only region.
	if ( Location >= 0 && Location <= 0x7FFF )
		return false;
	else
		return true;
}

void Z80::Write( uint16_t Location, uint16_t value )
{
	if ( !WriteAddressCheck( Location, ( uint8_t )value ) )
		return;

	//if ( Location >= 0xA000 && Location <= 0xBFFF && this->ram_enabled == false )
	//	return;

	// write first, i imagine.
	auto loc = this->GetRealAddress( Location );
	*( uint16_t* )loc = value;
}

void Z80::Write( uint16_t Location, uint8_t value )
{
	if ( !WriteAddressCheck( Location, ( uint8_t )value ) )
		return;

	// writes ignored.
	if ( Location >= 0xA000 && Location <= 0xBFFF )
	{
		if ( this->ram_enabled == false )
			return;
		else
		{
			auto loc = this->GetRealAddress( Location );
			if ( this->mbcType == MBCType::mbc2 )
				*loc = ( value & 0xF );
			else
				*loc = value;
			return;
		}
	}

	// write first, i imagine.
	auto loc = this->GetRealAddress( Location );
	//if ( Location >= 0x8000 && Location <= 0x9FFF )
	//	if ( value == 0x7F )
	//		Log( "Writing 7F at PC %X", this->Registers.pc );

	*loc = value;
}

uint8_t Z80::Read( uint16_t location )
{
	if ( !ReadAddressCheck( location ) )
		return 0xFF;
	return *this->GetRealAddress( location );
}

uint8_t* Z80::GetRealAddress( uint16_t addr )
{
	if ( addr >= 0 && addr <= 0x7FFF )
	{
		if ( this->dmg_enabled && addr <= 0xFF )
		{
			// read from bios start, bank 0, always.
			return ( uint8_t* )&this->pRam[ addr ];
		}
		else
		{
			int bank_number = 0;
			this->bank_bits.reserved = 0;

			if ( addr <= 0x3FFF )
				bank_number = this->mbc_mode ? ( this->bank_bits.bank_register_2 << 5 ) : 0;
			else if ( addr >= 0x4000 && addr <= 0x7FFF )
			{
				if( this->mbcType == MBCType::mbc3 )
					bank_number = this->bank_register; // maybe?
				else
					bank_number = this->bank_bits.bank_register_1; // maybe?

				auto new_addr = ( addr - 0x4000 ) + ( bank_number * 0x4000 );
				return ( uint8_t* )&this->pCartridge[ new_addr ]; // next X 16kb range
			}
			auto new_addr = ( addr & 0x3FFF ) + ( bank_number * 0x4000 );

			return ( uint8_t* )&this->pCartridge[ new_addr ]; // next X 16kb range
		}
	}
	else if ( addr >= 0xE000 && addr <= 0xFDFF )// echo of C->E0
		return ( uint8_t* )&this->pRam[ addr - 0x2000 ];
	else if ( this->ram_enabled && addr >= 0xA000 && addr <= 0xBFFF )
	{
		if ( this->mbcType == MBCType::mbc3 )
		{
			this->bank_bits.reserved = 0;

			auto new_addr = ( addr )+( this->ram_bank * 0x2000 );
			return ( uint8_t* )&this->pCartridge[ new_addr ];
		}
		else
			return ( uint8_t* )&this->pCartridge[ addr ];
	}
	else	// read from ram start.
		return ( uint8_t* )&this->pRam[ addr ];
}

// TODO: Not reading from ram/rambank yet.