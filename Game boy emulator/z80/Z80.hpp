#pragma once
#include "../GenericHeader.h"
#include "../gfx/gfx.hpp"

// Gameboy specced, not normal

union RegPair {
	uint16_t full;
	union {
		uint8_t split[ 2 ]; // 0 is lo bit 1 is hi bit
		struct {
			uint8_t second;
			uint8_t first;	// i.e. in hl , this will be h
		};
	};
};

struct CPURegisters {
	struct {
		RegPair bc;
		RegPair de;
		RegPair hl;
	} gp;

	union Flags {
		uint8_t full;
		struct {
			uint8_t ignored : 4;	// Not used
			uint8_t c : 1;	// carry flag
			uint8_t h : 1;	// half carry flag ( set if LS nibble overflows )
			uint8_t n : 1;	// add/sub flag
			uint8_t z : 1;	// zero flag
		};
	};

	RegPair af;

	uint16_t sp;	// Stack pointer
	uint16_t pc;	// Program Counter (eip)

	Flags* GetFlags( ) { return ( Flags* )&this->af.second; }
};

class Z80 {
	enum class SetInterrupt : uint8_t {
		no_change,
		disable,
		enable
	};

	CPURegisters Registers;
	SetInterrupt toggle_interrupts = SetInterrupt::no_change;
	bool ime = false; // interrupt master enable

	// For register-addressing
	std::pair< uint8_t*, uint8_t*> GetOperandsByIndex( int dst, int src, bool as_pair = false );
	void Pop( uint16_t * pDest );
	void Push( uint16_t value );

	// Increments PC by 1 after reading a byte
	uint8_t ReadPCByte( );
	// Increments PC by 2 after reading a word
	uint16_t ReadPCWord( );
	// Increments PC by 1 after reading a signed byte
	int8_t ReadSignedPCByte( );
	// Increments PC by 2 after reading a signed word
	int16_t ReadSignedPCWord( );

	// stuff like PUSH/POP
	int HandleMisc( uint8_t base_opcode, uint8_t opcode );
	int HandleInvalid( uint8_t base_opcode, uint8_t opcode ); // do nothing, stuff like 0xDD (IX instruction)
	int HandleINCDEC( uint8_t base_opcode, uint8_t opcode );
	int HandleLDPP( uint8_t base_opcode, uint8_t opcode );
	int HandleBranchChange( uint8_t base_opcode, uint8_t opcode );
	int HandleADD( uint8_t base_opcode, uint8_t opcode );
	int HandleSUB( uint8_t base_opcode, uint8_t opcode );
	int HandleXOR( uint8_t base_opcode, uint8_t opcode );
	int HandleAND( uint8_t base_opcode, uint8_t opcode );
	int HandleCMP( uint8_t base_opcode, uint8_t opcode );
	int HandleOR( uint8_t base_opcode, uint8_t opcode );
	int HandleBitwise( uint8_t base_opcode, uint8_t opcode );
	int HandleLDImm( uint8_t base_opcode, uint8_t opcode );
	int HandleLDReg( uint8_t base_opcode, uint8_t opcode );
	int HandleLDMem( uint8_t base_opcode, uint8_t opcode );
	int UnimplementedInstruction( uint8_t, uint8_t );
	int HandleLDSP( uint8_t, uint8_t );
	bool dmg_enabled = true;
	using fn = int( Z80::* )( uint8_t base_opcode, uint8_t opcode ); // appears to be how to do class-fn-pointers?

	struct OpcodeEntry {
		uint8_t opcode;
		union {
			uint8_t range;
			bool has_range;
		};

		fn Handler;

		OpcodeEntry( uint8_t first, uint8_t second, fn third )
		{
			this->opcode = first;
			this->range = second;
			this->Handler = third;
		}
	};

	OpcodeEntry opcode_array[ 142 ] =
	{
		// 8-Bit LD
		{ 0x76, false,  &Z80::HandleMisc  },
		{ 0x40, 0x7F,	&Z80::HandleLDReg },

		{ 0x3C,false,	&Z80::HandleINCDEC },
		{ 0x04,false,	&Z80::HandleINCDEC },
		{ 0x0c,false,	&Z80::HandleINCDEC },
		{ 0x14,false,	&Z80::HandleINCDEC },
		{ 0x1c,false,	&Z80::HandleINCDEC },
		{ 0x24,false,	&Z80::HandleINCDEC },
		{ 0x2c,false,	&Z80::HandleINCDEC },
		{ 0x34,false,	&Z80::HandleINCDEC },

		{ 0x05, false,	&Z80::HandleINCDEC  },	// B--
		{ 0x0D, false,	&Z80::HandleINCDEC  },	// C--
		{ 0x15, false,	&Z80::HandleINCDEC  },	// D--
		{ 0x1D, false,	&Z80::HandleINCDEC  },	// E--
		{ 0x25, false,	&Z80::HandleINCDEC  },	// H--
		{ 0x2D, false,	&Z80::HandleINCDEC  },	// L--
		{ 0x35, false,	&Z80::HandleINCDEC  },	// [HL]--
		{ 0x3D, false,	&Z80::HandleINCDEC  },	// A--

		{ 0x03, false,	&Z80::HandleINCDEC },	// BC++
		{ 0x13, false,	&Z80::HandleINCDEC },	// DE++
		{ 0x23, false,	&Z80::HandleINCDEC },	// HL++
		{ 0x33, false,	&Z80::HandleINCDEC },	// SP++

		{ 0x0b, false,	&Z80::HandleINCDEC },	// BC--
		{ 0x1b, false,	&Z80::HandleINCDEC },	// DE--
		{ 0x2b, false,	&Z80::HandleINCDEC },	// HL--
		{ 0x3b, false,	&Z80::HandleINCDEC },	// SP--

		// 8-Bit LD(D) Ext+Imm
		{ 0x3E, false,  &Z80::HandleLDImm },
		{ 0x06, false,  &Z80::HandleLDImm },
		{ 0x0E, false,  &Z80::HandleLDImm },
		{ 0x16, false,  &Z80::HandleLDImm },
		{ 0x1E, false,  &Z80::HandleLDImm },
		{ 0x26, false,  &Z80::HandleLDImm },
		{ 0x2E, false,  &Z80::HandleLDImm },
		{ 0x36, false,  &Z80::HandleLDImm },
		{ 0x3A, false,  &Z80::HandleLDMem },
		{ 0x32, false,  &Z80::HandleLDMem },		// LDD
		{ 0xE2, false,  &Z80::HandleLDMem },
		{ 0xE0, false,  &Z80::HandleLDMem },
		{ 0xEA, false,  &Z80::HandleLDMem },
		{ 0xFA, false,	&Z80::HandleLDMem },
		{ 0x1A, false,  &Z80::HandleLDMem },
		{ 0x22, false,  &Z80::HandleLDMem },
		{ 0xF0, false,  &Z80::HandleLDMem },
		{ 0xF2, false,  &Z80::HandleLDMem },
		{ 0x02, false,  &Z80::HandleLDMem },
		{ 0x12, false,  &Z80::HandleLDMem },
		{ 0x0A, false,  &Z80::HandleLDMem },
		{ 0x1A, false,  &Z80::HandleLDMem },
		{ 0xFA, false,  &Z80::HandleLDMem },

		/*
			16-Bit LD, PUSH, POP (ofc these modify sp)
		*/
		{ 0xE8, false,  &Z80::HandleLDPP  },
		{ 0x01, false,  &Z80::HandleLDPP  },
		{ 0x08, false,  &Z80::HandleLDPP  },
		{ 0xED, false,  &Z80::HandleLDPP  },			// USES 2ND BYTE ( like 3 variations )
		{ 0x11, false,  &Z80::HandleLDPP  },
		{ 0x21, false,  &Z80::HandleLDPP  },
		{ 0x2A, false,  &Z80::HandleLDPP  },
		{ 0x31, false,  &Z80::HandleLDPP  },
		{ 0xF8, false,  &Z80::HandleLDPP  },
		{ 0xF9, false,  &Z80::HandleLDPP  },
		/*
			8-Bit Arithmetic + Logic
		*/

		{ 0x09, false,	&Z80::HandleADD		 },			// ADD HL
		{ 0x19, false,	&Z80::HandleADD		 },			// ADD HL
		{ 0x29, false,	&Z80::HandleADD		 },			// ADD HL
		{ 0x39, false,	&Z80::HandleADD		 },			// ADD HL
		{ 0x80, 0x87,	&Z80::HandleADD		 },			// ADD
		{ 0x88, 0x8F,	&Z80::HandleADD		 },			// ADC
		{ 0xC6, false,	&Z80::HandleADD		 },			// ADD A, (n)
		{ 0xCE, false,  &Z80::HandleADD		 },			// ADC A, (n)
		{ 0x90, 0x97,	&Z80::HandleSUB		 },			// SUB + SBC // likewise
		{ 0x98, 0x9F,	&Z80::HandleSUB		 },			// SUB + SBC // likewise
		{ 0xDE, false,  &Z80::HandleSUB		 },			// SBC #
		{ 0xD6, false,  &Z80::HandleSUB		 },			// SUB (n)
		//{ 0xB8, false,  &Z80::HandleLDPP	 },			// CMP (cp)
		{ 0x04, false,  &Z80::HandleLDPP	 },			// different scaling on incs + decs
		{ 0xA0, 0xA7,	&Z80::HandleAND		 },			// AND
		{ 0xA8, 0xAF,	&Z80::HandleXOR		 },			// XOR
		{ 0xB0, 0xB7,	&Z80::HandleOR		 },			// OR (n)
		{ 0xB8, 0xBF,	&Z80::HandleCMP		 },			// CMP REG
		{ 0xEE, false,	&Z80::HandleXOR		 },			// XOR
		{ 0x07, false,  &Z80::HandleBitwise	 },			// RLCA
		{ 0x17, false,  &Z80::HandleBitwise	 },			// RLA
		{ 0x0F, false,  &Z80::HandleBitwise	 },			// RRCA
		{ 0x1F, false,  &Z80::HandleBitwise	 },			// RRA
		//{ 0xBE, false,  &Z80::HandleCMP		 },			// CMP (HL)
		{ 0xFE, false,  &Z80::HandleCMP		 },			// CMP nn
		{ 0xE6, false,  &Z80::HandleAND		 },
		{ 0xF6, false,  &Z80::HandleOR		 },

		/*
			Branch Instructions
		*/
		// this ones kind of aids.
		{ 0xE9, false,  &Z80::HandleBranchChange  },				// JMP (HL)
		{ 0xC3, false,  &Z80::HandleBranchChange  },			// JMP
		{ 0xC2, false,  &Z80::HandleBranchChange  },			// JMP NZ
		{ 0xCA, false,  &Z80::HandleBranchChange  },			// JMP Z
		{ 0xD2, false,  &Z80::HandleBranchChange  },			// JMP NC
		{ 0xDA, false,  &Z80::HandleBranchChange  },			// JMP C
		{ 0x18, false,  &Z80::HandleBranchChange  },			// JMP relative
		{ 0x20, false,  &Z80::HandleBranchChange  },			// JR NZ
		{ 0x28, false,  &Z80::HandleBranchChange  },			// JR Z
		{ 0x30, false,  &Z80::HandleBranchChange  },			// JR NC
		{ 0x38, false,  &Z80::HandleBranchChange  },			// JR C
		{ 0xCD, false,  &Z80::HandleBranchChange  },			// CALL
		{ 0xC4, false,  &Z80::HandleBranchChange  },			// CALL NZ
		{ 0xCC, false,  &Z80::HandleBranchChange  },			// CALL Z
		{ 0xD4, false,  &Z80::HandleBranchChange  },			// CALL NC
		{ 0xDC, false,  &Z80::HandleBranchChange  },			// CALL C
		{ 0xC0, false,  &Z80::HandleBranchChange  },			// RET NZ
		{ 0xC8, false,  &Z80::HandleBranchChange  },			// RET Z
		{ 0xD0, false,  &Z80::HandleBranchChange  },			// RET NC
		{ 0xC9, false,  &Z80::HandleBranchChange  },			// RET
		{ 0xD8, false,	&Z80::HandleBranchChange  },			// RET C
		{ 0xD9, false,	&Z80::HandleBranchChange  },			// RETI
		/*
			Reserved memory location branch changes
		*/
		{ 0xC7, false,	&Z80::HandleBranchChange },				// RST 0x00
		{ 0xCF, false,	&Z80::HandleBranchChange },				// RST 0x08
		{ 0xD7, false,	&Z80::HandleBranchChange },				// RST 0x10
		{ 0xDF, false,	&Z80::HandleBranchChange },				// RST 0x18
		{ 0xE7, false,	&Z80::HandleBranchChange },				// RST 0x20
		{ 0xEF, false,	&Z80::HandleBranchChange },				// RST 0x28
		{ 0xF7, false,	&Z80::HandleBranchChange },				// RST 0x30
		{ 0xFF, false,	&Z80::HandleBranchChange },				// RST 0x38

		/*
			CB Prefixed instructions
		*/
		{ 0xCB, false,  &Z80::HandleBitwise },
		/*
			Misc instructions
		*/
		{ 0x2F, false,	&Z80::HandleMisc  },						// CPL ( Complement accumulator )
		{ 0xC5, false,  &Z80::HandleMisc  },					// PUSH BC
		{ 0xD5, false,  &Z80::HandleMisc  },					// PUSH DE
		{ 0xE5, false,  &Z80::HandleMisc  },					// PUSH HL
		{ 0xF5, false,  &Z80::HandleMisc  },					// PUSH AF
		{ 0xC1, false,  &Z80::HandleMisc  },					// POP BC
		{ 0xD1, false,  &Z80::HandleMisc  },					// POP DE
		{ 0xE1, false,  &Z80::HandleMisc  },					// POP HL
		{ 0xF1, false,  &Z80::HandleMisc  },					// POP AF
		{ 0x00, false,  &Z80::HandleMisc  },					// NOP
		{ 0xF3, false,  &Z80::HandleMisc  },					// Disable Interrupt (after instruction after this one)
		{ 0xFB, false,  &Z80::HandleMisc  },					// Enable Interrupt (after instruction after this one)
		{ 0x37, false,  &Z80::HandleMisc  },					// SCF (Set carry flag)
		{ 0x3F, false,  &Z80::HandleMisc  },					// CCF ( Complement carry flag)
		{ 0x10, false,  &Z80::HandleMisc  },					// STOP ( 10 00 )	
		{ 0x27, false,  &Z80::HandleMisc  },					// DAA ( Decimal Adjust A )
		/*
			Invalid instructions (doing this so blarrgs test will run)
		*/
		{ 0xD3, false,  &Z80::HandleInvalid },
		{ 0xDB, false,  &Z80::HandleInvalid },
		{ 0xDD, false,  &Z80::HandleInvalid },
		{ 0xE3, false,  &Z80::HandleInvalid },
		{ 0xE4, false,  &Z80::HandleInvalid },
		{ 0xEB, false,  &Z80::HandleInvalid },
		{ 0xEC, false,  &Z80::HandleInvalid },
		{ 0xED, false,  &Z80::HandleInvalid },
		{ 0xF4, false,  &Z80::HandleInvalid },
		{ 0xFC, false,  &Z80::HandleInvalid },
		{ 0xFD, false,  &Z80::HandleInvalid },
	};

	void HandleInterrupts( );
	int GetDestRegisterIndex( int full_op );
	int GetSrcRegisterIndex( int full_op );

	// pData/pRam is getting corrupted.
	union {
		unsigned char * pData = nullptr; // data and ram are meant to be the same thing!
		char * pRam;
		MemoryArea_s* pMem;
	};

	char * pCartridge = nullptr;
	int elapsed_cycles = 0;
	bool halted = false;
	bool stopped = false;

	bool ram_enabled = false;
	union {
		uint8_t bank_register = 1; // used to decide what bank to use to access 0x4000->0x7FFF
		struct {
			uint8_t bank_register_1 : 5;
			uint8_t bank_register_2 : 2;
			uint8_t reserved : 1;
		} bank_bits;
	};
	uint8_t ram_bank = 0;

	bool mbc_mode = false; // 1 == affects 0->7FFF + 0xA000->0xBFFF. 0 == affects 0x4000->0x7FFF
	CartridgeType cartType;
	enum class MBCType :uint8_t {
		none,
		mbc1,
		mbc2,
		mbc3,
	};

	MBCType mbcType;


	// REFACTOR TIME :((
	bool ReadAddressCheck( uint16_t Location );
	bool WriteAddressCheck( uint16_t addr, uint8_t lowval );
	void Write( uint16_t Location, uint16_t value );
	void Write( uint16_t Location, uint8_t value );
public:
	// Exposed for things like GPU
	uint8_t Read( uint16_t location );
	void SetCartridge( char * cart, CartridgeType type );
	void SetRam( char * ram );
	bool Run( unsigned char * data );
	int Step( );
	bool IsStopped( ); // LCD is disabled during STOP but not halt.
	bool IsHalted( );
	bool IsDMGEnabled( );
	char * GetCartridge( );
	// make life easy.
	uint8_t * GetRealAddress( uint16_t addr );


	CPURegisters * GetRegisters( );
};