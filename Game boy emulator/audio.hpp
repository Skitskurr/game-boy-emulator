#pragma once
#include "GenericHeader.h"

class GameBoy;

class Audio { // im so creative

	struct NR10_SweepRegister {
		uint8_t num_shifts : 3; // 0 ( number of sweep shift ( 0-7 ) )
		uint8_t sweep_type : 1; // 3 ( 0 = addition, frequency increases )
		uint8_t sweep_time : 3; // 4 ( theres a table for values on pandocs, but 0 = sweep off. )
	};

	struct NR11_Duty {
		uint8_t sound_length_data : 6; // 0 (64-value) * (1/256) seconds. Only usable if bit 6 in nr14 set
		uint8_t wave_pattern_duty : 2; // 6 some weird percentage thing idk
	};

	struct NR12_VolumeEnvelope {
		uint8_t num_envelope_sweeps : 3;	// 0 ( number of envelope sweep 0-7, if 0, no envelope operations )
		uint8_t envelope_direction : 1;		// 3 ( 0 = decrease, 1 = increase. Thanks for keeping this the same as NR10 gameboy. )
		uint8_t initial_volume : 4;			// 4 ( Initial volume, duh. 0-0F, 0 = no sound )
	}; // 1 step = num_sweeps*(1/64) seconds

	struct NR13_FrequencyLo {
		uint8_t lo_frequency; // lower 8 bits of 11 bit frequency (continued in nr14)
	};

	struct NR14_FrequencyHi {
		uint8_t hi_frequency : 3; // 0 Real frequency = 131072/(2048-this+lo_frequency) Hz
	private:
		uint8_t superpad : 3; // 3
	public:
		uint8_t counter_selection : 1;	// 6 ( idk )
		uint8_t initial : 1;			// 7 ( 1 = restart sound )
	};

	struct channel_one {
		NR10_SweepRegister nr_10;
		NR11_Duty nr_11;
		NR12_VolumeEnvelope nr_12;
		NR13_FrequencyLo nr_13;
		NR14_FrequencyHi nr_14;
	};

	channel_one * pSound1;

	GameBoy * pParent;
public:
	Audio( GameBoy * );

	void Step( int cycles );
};