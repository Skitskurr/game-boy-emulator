#include "audio.hpp"
#include "GameBoy.hpp"

Audio::Audio( GameBoy * pParent )
{ 
	this->pParent = pParent;
	auto ram = ( MemoryArea_s* )this->pParent->GetRam( );
	this->pSound1 = ( decltype( this->pSound1 ) )&ram->big_union.nr_10;
}

void Audio::Step( int cycles )
{ 
	if ( this->pSound1->nr_10.sweep_time )
	{
		Log( "Should be sweeping!" );
	}
}